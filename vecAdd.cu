extern "C" __global__ void vecAdd(float *h_a, float *h_b, float *h_r,
                                  int VectorSize) {
  // Get our global thread ID
  int id = blockIdx.x * blockDim.x + threadIdx.x;

  if (id < VectorSize) {
    h_r[id] = h_a[id] + h_b[id];
    // printf("h_r[%d]= h_a[%d]=%f + h_b[%d]=%f = %f
    // \n",id,id,h_a[id],id,h_b[id],h_r[id]);
  }
}
