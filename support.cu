
#define LLC_REDUCTION_FUNC(dest, fuente) dest = dest + fuente;
struct SharedMemory_double {
  __device__ inline operator double *() {
    extern __shared__ double __smem[];
    return (double *)__smem;
  }

  __device__ inline operator const double *() const {
    extern __shared__ double __smem[];
    return (double *)__smem;
  }
};

struct SharedMemory_float {
  __device__ inline operator float *() {
    extern __shared__ float __smem_f[];
    return (float *)__smem_f;
  }

  __device__ inline operator const float *() const {
    extern __shared__ float __smem_f[];
    return (float *)__smem_f;
  }
};
struct SharedMemory_int {
  __device__ inline operator int *() {
    extern __shared__ int __smem_i[];
    return (int *)__smem_i;
  }

  __device__ inline operator const int *() const {
    extern __shared__ int __smem_i[];
    return (int *)__smem_i;
  }
};
extern "C" __global__ void set_value(double *data, int value) {
  // unsigned int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  data[idx] = 1.0;
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void reduce_double2(double *g_idata, double *g_odata,
                                          int n) {

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int i = idx;
  g_idata[i] = 7.0;
  g_odata[i] = 4.0;
  /*
       // if (i >= 0 && i < n) {
          g_odata[i] = (double) 0.0; // 3.33;
          g_idata[i] = (double) 0.0; // 4.44;
       // }
  */
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void reduction(double *g_idata, double *g_odata, int n) {
  // extern __shared__ double sdata[];
  double *sdata = SharedMemory_double();

  // perform first level of reduction,
  // reading from global memory, writing to shared memory
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x * (blockDim.x * 2) + threadIdx.x;

  sdata[tid] = (i < n) ? g_idata[i] : 0;
  if (i + blockDim.x < n)
    /* sdata[tid] += g_idata[i+blockDim.x]; */
    LLC_REDUCTION_FUNC(sdata[tid], g_idata[i + blockDim.x]);

  __syncthreads();

  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      /*           sdata[tid] += sdata[tid + s];*/
      LLC_REDUCTION_FUNC(sdata[tid], sdata[tid + s]);
    }
    __syncthreads();
  }

  // write result for this block to global mem
  if (tid == 0)
    g_odata[blockIdx.x] = sdata[0];
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void final_double(double *g_idata, double *g_odata,
                                        int blocks) {
  for (int i = 1; i < blocks; i++) {
    g_idata[0] += g_idata[i];
  }
  *g_odata += g_idata[0];
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void final_float(float *g_idata, float *g_odata,
                                       int blocks) {

  for (int i = 1; i < blocks; i++) {
    g_idata[0] += g_idata[i];
  }
  *g_odata += g_idata[0];
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void final_int(int *g_idata, int *g_odata, int blocks) {
  for (int i = 1; i < blocks; i++) {
    g_idata[0] += g_idata[i];
  }
  *g_odata += g_idata[0];
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void reduction_float(float *g_idata, float *g_odata,
                                           int n) {
  // extern __shared__ double sdata[];
  float *sdata = SharedMemory_float();

  // perform first level of reduction,
  // reading from global memory, writing to shared memory
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x * (blockDim.x * 2) + threadIdx.x;

  sdata[tid] = (i < n) ? g_idata[i] : 0;
  if (i + blockDim.x < n)
    /* sdata[tid] += g_idata[i+blockDim.x]; */
    LLC_REDUCTION_FUNC(sdata[tid], g_idata[i + blockDim.x]);

  __syncthreads();

  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      /*           sdata[tid] += sdata[tid + s];*/
      LLC_REDUCTION_FUNC(sdata[tid], sdata[tid + s]);
    }
    __syncthreads();
  }

  // write result for this block to global mem
  if (tid == 0)
    g_odata[blockIdx.x] = sdata[0];
}

// original de ruyman pero es el algoritmo numero 4
extern "C" __global__ void reduction_int(int *g_idata, int *g_odata, int n) {
  // extern __shared__ double sdata[];
  int *sdata = SharedMemory_int();

  // perform first level of reduction,
  // reading from global memory, writing to shared memory
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x * (blockDim.x * 2) + threadIdx.x;

  sdata[tid] = (i < n) ? g_idata[i] : 0;
  if (i + blockDim.x < n)
    /* sdata[tid] += g_idata[i+blockDim.x]; */
    LLC_REDUCTION_FUNC(sdata[tid], g_idata[i + blockDim.x]);

  __syncthreads();

  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      /*           sdata[tid] += sdata[tid + s];*/
      LLC_REDUCTION_FUNC(sdata[tid], sdata[tid + s]);
    }
    __syncthreads();
  }

  // write result for this block to global mem
  if (tid == 0)
    g_odata[blockIdx.x] = sdata[0];
}
