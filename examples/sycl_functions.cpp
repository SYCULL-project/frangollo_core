/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cstring>
#include <frangollo_core/interface/syclFunctions.h>
#include <iostream>

frgsycl::FrangolloRuntime FrgRuntime;

#define LENGTH (16) // Length of vectors
#define TOL (0.001)

int main() {

  float h_a[LENGTH]; // a vector
  float h_b[LENGTH]; // b vector
  float h_r[LENGTH]; // r vector (result)

  int VectorSize = LENGTH;

  for (int i = 0; i < LENGTH; i++) {
    h_a[i] = rand() / (float)RAND_MAX;
    h_b[i] = rand() / (float)RAND_MAX;
  }

  int regionId = FrgRuntime.createSimpleContext("vecAdd");
  FrgRuntime.setActiveContext(regionId);

  { // registering variables in device
    FrgRuntime.registerArrayVar(&h_a, regionId, frgsycl::IN,
                              frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                              sizeof(float), VectorSize);
    FrgRuntime.registerArrayVar(&h_b, regionId, frgsycl::IN,
                              frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                              sizeof(float), VectorSize);
    FrgRuntime.registerArrayVar(&h_r, regionId, frgsycl::INOUT,
                              frgsycl::FORCE_COPY_IN, frgsycl::WRITABLE,
                              sizeof(float), VectorSize);
    FrgRuntime.registerScalarVar(&VectorSize, regionId, frgsycl::IN,
                               frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                               sizeof(int));
  }

  tparams *vecAdd_parameter_list = NULL;
  {
    FrgRuntime.addParamToList(vecAdd_parameter_list, &h_a, frgsycl::ARRAY,
                      frgsycl::READ_ONLY, sizeof(float) * LENGTH,
                      (char *)"h_a");
    FrgRuntime.addParamToList(vecAdd_parameter_list, &h_b, frgsycl::ARRAY,
                      frgsycl::READ_ONLY, sizeof(float) * LENGTH,
                      (char *)"h_b");
    FrgRuntime.addParamToList(vecAdd_parameter_list, &h_r, frgsycl::ARRAY,
                      frgsycl::WRITABLE, sizeof(float) * LENGTH, (char *)"h_r");
    FrgRuntime.addParamToList(vecAdd_parameter_list, &VectorSize, frgsycl::SCALAR,
                      frgsycl::READ_ONLY, sizeof(int), (char *)"VectorSize");
  }

  FrgRuntime.submit_kernel_auto(FrgRuntime.getActiveContext(), "vecAdd",
                              vecAdd_parameter_list, VectorSize, -1, -1);

  sycl_device_t deviceType = sycl_device_none;
  deviceType = FrgRuntime.get_device_type();
  if (deviceType == sycl_device_nvidia)
    std::cout << "The device type is CUDA " << std::endl;
  else
    std::cout << "Other device type" << std::endl;  

  FrgRuntime.destroyContext(regionId);
  

  // Test the results
  int correct = 0;
  float tmp;
  for (int i = 0; i < VectorSize; i++) {
    tmp = h_a[i] + h_b[i]; // assign element i of a+b to tmp
    tmp -= h_r[i];         // compute deviation of expected and output result
    if (tmp * tmp <
        TOL *
            TOL) { // correct if square deviation is less than tolerance squared
      correct++;
    } else {
      printf(" tmp %f h_a %f h_b %f h_r %f \n", tmp, h_a[i], h_b[i], h_r[i]);
    }
  }
  // summarize results
  printf("R = A+B: %d out of %d results were correct.\n", correct, VectorSize);

  return 0;
}
