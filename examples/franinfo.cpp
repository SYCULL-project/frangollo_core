/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <frangollo_core/System.h>
#include <frangollo_core/common/Device.h>
#include <frangollo_core/common/Region.h>
#include <frangollo_core/common/Var.h>
#include <frangollo_core/cuda/CudaDevice.h>

using namespace std;

int main() {
  /**** Extract system information

      System is a global singleton, constructed at program
          run and destroyed at the end.
  ****/
  vector<common::Device *> devList = common::system.getDeviceList();
  vector<common::Device *>::iterator it = devList.begin();
  cout << " Available Frangollo Devices: " << endl;
  for (; it != devList.end(); it++)
    cout << " --> " << (*it)->getDeviceString() << endl;

  cout << " CUDA Platform enabled " << endl;
  it = devList.begin();
  for (; it != devList.end(); it++)
    if ((*it)->getDeviceString() == cuda::CUDA_STRING) {
      cout << "CUDA device use the following HW component " << endl;
      cout << "--> " << (*it)->getHWDeviceString() << endl;
    }
  cout << " Generic memory cleanup " << std::endl;
}
