/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <frangollo_core/common/Var.h>
#include <frangollo_core/cuda/CudaDevice.h>
#include <unistd.h>
#include <vector>

#define ALIGN_UP(offset, alignment)                                            \
  (offset) = ((offset) + (alignment)-1) & ~((alignment)-1)

#define min(a, b) (a > b ? b : a)
#define max(a, b) (a > b ? a : b)

namespace cuda {

CUmodule loadPtx(std::string name);

inline unsigned int nextPow2(unsigned int &x) {
  --x;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return ++x;
}

inline unsigned int prevPow2(const unsigned int &x) {
  unsigned int i = 4;
  unsigned int p = i;
  for (; i <= x; i *= 2)
    p = i;

  return p;
}

inline int roundUp(const int &numToRound, const int &multiple) {
  if (multiple == 0) {
    return numToRound;
  }

  int remainder = numToRound % multiple;
  if (remainder == 0) {
    return numToRound;
  }

  return numToRound + multiple - remainder;
}

inline unsigned CudaDevice::warpsPerMP(void) {
  return computeCapability.first == 3
             ? 64u
             : computeCapability.first == 2
                   ? 48u
                   : computeCapability.second >= 2 ? 32u : 24u;
  /*    if (computeCapability.first == 1) {
         if ((computeCapability.second == 2) ||
               (computeCapability.second == 3))
              return 32u;
         else
              return 24u;
      } else {
         return 48u;
      }*/
}

inline unsigned CudaDevice::regsPerMP(void) {
  return computeCapability.first == 3
             ? 65536u
             : computeCapability.first == 2
                   ? 32000u
                   : computeCapability.second >= 2 ? 16000u : 8000u;
  /*    if (computeCapability.first == 1) {
         if ((computeCapability.second == 2) ||
               (computeCapability.second == 3))
              return 16000u;
         else
              return 8000u;
      } else {
         return 32000u;
      }*/
}

inline float CudaDevice::computeOccupancy(const unsigned &nThreads,
                                          const unsigned &nRegs,
                                          const unsigned &shMem) {
  /* See CUDA Occupancy Calculator and Section 4.2 of Cuda programming guide
        to understand this equations */
  unsigned Wblock = ceil(nThreads / this->_props.SIMDWidth);
  unsigned Rblock, Gt;
  unsigned Wallowed, Rallowed, ActThBlock, ActWarpMP, ActThPerMP;
  if (computeCapability.first == 1) {
    /* Extract the granularity of register allocation */
    /*     if (computeCapability.second == 0 || computeCapability.second == 1)
            Gt = 256;
         else // if (computeCapability.second == 2 || computeCapability.second
       == 3)
            Gt = 512;*/
    Gt = computeCapability.second >= 2 ? 512 : 256;
    Rblock = roundUp(roundUp(Wblock, 2) * this->_props.SIMDWidth * nRegs, Gt);
  } else if (computeCapability.first == 2) {
    Gt = 64;
    Rblock = roundUp(nRegs * this->_props.SIMDWidth, Gt) * Wblock;
  } else if (computeCapability.first == 3) {
    Gt = 256;
    Rblock = roundUp(nRegs * this->_props.SIMDWidth, Gt) * Wblock;
  } else {
    std::cerr << " Unknown compute capability " << this->computeCapability.first
              << " - " << this->computeCapability.second << std::endl;
    return nThreads;
  }
  Wallowed = min(blocksPerMP, floor(warpsPerMP() / Wblock));
  Rallowed = nRegs > 0 ? floor(regsPerMP() / Rblock) : blocksPerMP;
  ActThBlock = min(Wallowed, Rallowed);
  ActWarpMP = ActThBlock * Wblock;
  ActThPerMP = ActWarpMP * (this->_props.SIMDWidth);
  float occupancy = (((float)ActWarpMP / (float)warpsPerMP()) * 100.0);

  // DEBUGLN("Wblock %d , Wallowed %d, Rblock %d, Rallowed %d, nThreads %d, \%
  // Occupancy %f \n", Wblock, Wallowed, Rblock, Rallowed, nThreads,occupancy );

  return occupancy;
}

inline unsigned CudaDevice::maximizeOccupancy(const unsigned &nThreads,
                                              const unsigned &nRegs,
                                              const unsigned &shMem) {
  float max = 0.f;
  float tmp = 0.f;
  unsigned threads;
  unsigned m_of_warp = nThreads / this->_props.SIMDWidth;
  /* Explore solution space from  m_of_warp/2 : nThreads : m_of_warp/2*/
  unsigned i = ((m_of_warp) > 1) ? m_of_warp / 2 : m_of_warp;
  unsigned maxWarpMultiple =
      (this->_props.maxThreadsPerBlock / this->_props.SIMDWidth);
  float maximum = this->computeOccupancy(nThreads, nRegs, shMem);
  // A 60% of occupancy is enough, according to CUDA optimization guidelines
  if (maximum > 60.0f)
    return nThreads;

  i = max(1, i);
  DEBUGLN("**Initial %d Exploring from: %d to %d \n", nThreads, i,
          (maxWarpMultiple));
  for (; i < (maxWarpMultiple); i++) {
    tmp = this->computeOccupancy(this->_props.SIMDWidth * i, nRegs, shMem);
    // DEBUGLN("**Occ is %f for %d \n", tmp, this->_props.SIMDWidth * i);
    if (tmp > maximum) {
      maximum = tmp;
      threads = this->_props.SIMDWidth * i;
      // Same 60% rule
      if (maximum > 60)
        break;
    }
  }
  // DEBUGLN(" Max. Occ is %f with %d threads (original: %d) \n",   max,
  // threads, nThreads);
  return threads;
}

inline unsigned CudaDevice::estimateNumThreads(const unsigned &numMemAccess,
                                               const unsigned &numFlop,
                                               const unsigned &numRegs) {
  if (this->getThreadNumber() > 0) {
    // printf("** Threads fixed to : %d \n " , this->getThreadNumber());
    return this->getThreadNumber();
  } else {

    /*
       A memory operation requires FACTOR times the time of a Floating point
       operation,
       this means that the more MemAcc we have in correlation with FLOP, the
       less memory
       access we want in the same warp */
    const float FACTOR = this->getGranularityFactor();
    const int maxPlatform = this->_props.maxThreadsPerBlock;
    unsigned int sharedMemory = 0;
    float numThreads =
        maxPlatform * (((float)numFlop / ((float)numMemAccess * FACTOR)));
    unsigned int iNumThreads = (unsigned int)numThreads;
    iNumThreads = nextPow2(iNumThreads);
    iNumThreads = iNumThreads > maxPlatform ? maxPlatform : iNumThreads;
    iNumThreads = max(iNumThreads, this->_props.SIMDWidth);

    unsigned finalThreads =
        this->maximizeOccupancy(iNumThreads, numRegs, sharedMemory);
    //		printf("** Calculated value : %d \n", finalThreads);
    if (this->computeCapability.first >= 3)
      return min(64, finalThreads);

    return finalThreads;
  }
}

void CudaDevice::init() {

  /* Quick return if possible */
  if (this->_isOnline)
    return;

  int deviceCount = 0;

  DEBUGLN(" Initialization of CUDA device \n");
  CHECK_ERROR(cuInit(0));

  try {
    CHECK_ERROR(cuDeviceGetCount(&deviceCount));
  } catch (CudaError &ce) {
    std::cerr << "No device available, abort " << std::endl;
    exit(1);
  }

  // std::cout << " Detected " << deviceCount << " cuda enabled devices \n" <<
  // std::endl;

  int major = 0;
  int newMajor;
  int minor = 0;
  int newMinor;
  int bestDevId;

  /* Choose best device */
  for (int devId = 0; devId < deviceCount; devId++) {
    cuDeviceComputeCapability(&newMajor, &newMinor, devId);
    if (newMajor > major) {
      major = newMajor;
      minor = newMinor;
      bestDevId = devId;
    } else if (minor > newMinor) {
      minor = newMinor;
      bestDevId = devId;
    }
  }
  /* Store compute capability information on Device instance */
  this->computeCapability.first = major;
  this->computeCapability.second = minor;
  char dev_name[512];
  cuDeviceGetName(dev_name, 512, bestDevId);
  // std::cout << " We have chosen : " << dev_name << std::endl;
  // std::cout << "  -> Cap. : " << this->computeCapability.first  << "." <<
  // minor << std::endl;

  // Get a pointer to the device
  CHECK_ERROR(cuDeviceGet(&_cuDevice, bestDevId));
  // Extract device information
  CHECK_ERROR(cuDeviceGetProperties(&_props, _cuDevice));

  // Create a working context
  CHECK_ERROR(cuCtxCreate(&_cuContext, 0, _cuDevice));
  this->_isOnline = true;
  // Load the support module (reduction functions)
  std::string aux = "support.ptx";
  CUmodule sModule = loadPtx(aux);
  this->setSupportModule(&sModule);
}

int CudaDevice::kernel_launch(common::Region *region, std::string name,
                              tparams *params, int size_x, int size_y,
                              int size_z) {
  tparams *paramPtr = params;
  int threadsPerBlock_x, threadsPerBlock_y, blocksPerGrid_x, blocksPerGrid_y;
  int N_x, N_y;
  void *param;
  common::HostVar *h_var;
  common::Var *d_var;
  int offset = 0;
  std::vector<common::var_pair> vars;

  int size = -1;
  if (size_y > 0) {
    size = size_x * size_y;
    N_x = max(size_x, 1);
    N_y = max(size_y, 1);
  } else {
    size = size_x;
    N_x = max(size_x, 1);
    N_y = 1;
  }

  if (size_z > 0)
    std::cout << " 3D KERNELS ARE NOT SUPPORTED INSIDE CUDA COMPONENT "
              << std::endl;

  /* Loading the kernel function */
  CUfunction func;
  DEBUGLN("cuModuleGetFunction (_cuModule %d) - function: %s", _cuModule,
          name.c_str());

  std::map<std::string, CUmodule>::iterator it = _cuModules.find(name);
  if (it == _cuModules.end()) {
    DEBUGLN("Error! The %s function can not be found in the list of cuModules",
            name.c_str());
  } else {
    DEBUGLN("The %s function was found in the list of cuModules", name.c_str());
  }
  CHECK_ERROR(
      cuModuleGetFunction(&func, _cuModules.find(name)->second, name.c_str()));
  DEBUGLN("Kernel module file %s has been loaded", name.c_str());

  /* Getting the kernel parameteres */
  while (paramPtr) {
    if (paramPtr->type == common::SCALAR && strlen(paramPtr->reduction) == 0) {
      // Scalar variables
      if (paramPtr->size > sizeof(float))
        ALIGN_UP(offset, __alignof(double));
      else
        ALIGN_UP(offset, __alignof(float)); // Vale para enteros tambien
      CHECK_ERROR(
          cuParamSetv(func, offset, paramPtr->_base_ptr, paramPtr->size));
      offset += paramPtr->size;
    } else {
      param = paramPtr->_base_ptr;
      h_var = region->findHostVar(param);
      d_var = region->findDeviceVar(param);
      if (d_var->isEmpty() && !strlen(paramPtr->reduction) &&
          (h_var->getDirection() == common::IN ||
           h_var->getDirection() == common::INOUT)) {

        DEBUGLN(">>>>> Since d_var was empty, we copy host var to it \n");
        d_var->copy_in(h_var);
      } else if (!strlen(paramPtr->reduction)) {
        DEBUGLN(">>>>> d_var was not empty \n");
      } else {
        DEBUGLN(
            ">>>>> d_var was a reduction var and it is treated separately \n");
      }
      d_var->inUse();
      d_var->inUse();
      if (h_var->getDirection() == common::OUT ||
          h_var->getDirection() == common::INOUT) {
        d_var->dirty();
        common::Var *father = d_var->getParent();
        if (father)
          father->dirty();
      }
      if (strlen(paramPtr->reduction) != 0) {
        // Reduction variables
        CudaReductionVar *red_var = new CudaReductionVar();
        DEBUGLN("\n\nVariable %p has a new child %p\n", d_var, red_var);
        red_var->setParent(d_var);
        red_var->setDevice(this);
        red_var->setType(common::ARRAY);
        red_var->setSize(size * h_var->getSize());
        red_var->setCopyType(common::FORCE_COPY_IN);
        red_var->setBaseType(paramPtr->reductionType);
        red_var->setOperation(paramPtr->reduction);
        red_var->setIdentity(paramPtr->identity);
        red_var->alloc();
        d_var->addChild(red_var);
        vars.push_back(std::make_pair(h_var, red_var));
        CUdeviceptr *aux = (CUdeviceptr *)red_var->getPtr();
        ALIGN_UP(offset, __alignof(aux));
        CHECK_ERROR(cuParamSetv(func, offset, aux, sizeof(aux)));
        offset += sizeof(aux);
      } else {
        // Array variables
        d_var->setDevice(this);
        d_var->setSize(h_var->getSize());
        d_var->setType(paramPtr->type);
        CUdeviceptr *aux = (CUdeviceptr *)d_var->getPtr();
        ALIGN_UP(offset, __alignof(aux));
        CHECK_ERROR(cuParamSetv(func, offset, aux, sizeof(aux)));
        offset += sizeof(aux);
      }
    }
    paramPtr = paramPtr->next;
  } // while params
  CHECK_ERROR(cuParamSetSize(func, offset));

  int numRegs;
  CHECK_ERROR(cuFuncGetAttribute(&numRegs, CU_FUNC_ATTRIBUTE_NUM_REGS, func))
  // Increase cache size if possible
  if (computeCapability.first >= 2)
    CHECK_ERROR(cuFuncSetCacheConfig(func, CU_FUNC_CACHE_PREFER_L1));

  Device::pre_kernel_launch(region, name);
  if (size_y < 0) {
    DEBUGLN("... Starting the launch of a kernel with one dimension\n");

    threadsPerBlock_x =
        max(8, this->estimateNumThreads(
                   _kernelInfo.find(name)->second.numMemoryAccess,
                   _kernelInfo.find(name)->second.numFlop, numRegs));
    // blocksPerGrid_x =  max(1,(N_x + threadsPerBlock_x - 1) /
    // threadsPerBlock_x);
    blocksPerGrid_x = max(1, (N_x - 1) / threadsPerBlock_x + 1);

    DEBUGLN("Max Grid size = %d \n", this->_props.maxGridSize[0]);
    // Only for parallel regions
    // TODO: Fix this for Kernels regions
    if (blocksPerGrid_x > this->_props.maxGridSize[1])
      blocksPerGrid_x = this->_props.maxGridSize[1];

    DEBUGLN("All the calculationes have been done. ThreadsPerBlock: %d. "
            "BlocksPerGrid: %d. Kernel launch...\n",
            threadsPerBlock_x, blocksPerGrid_x);
    blocksPerGrid_y = 1;
    threadsPerBlock_y = 1;
  } else {
    DEBUGLN("... Starting the launch of a kernel with two dimensions\n");
    // For 2D kernels, each dimension is sqrt the total threads (as grid is
    // estimatedThreads**2 )
    unsigned int estimatedThreads = this->estimateNumThreads(
        _kernelInfo.find(name)->second.numMemoryAccess,
        _kernelInfo.find(name)->second.numFlop, numRegs);
    unsigned int tmp = (unsigned int)sqrt((float)estimatedThreads);
    // To further optimize, we look for the previous power of two value
    estimatedThreads = max(4, prevPow2(tmp));

    threadsPerBlock_x = estimatedThreads;
    // blocksPerGrid_x =  (N_x + threadsPerBlock_x - 1) / threadsPerBlock_x;
    blocksPerGrid_x = (N_x - 1) / threadsPerBlock_x + 1;

    threadsPerBlock_y = estimatedThreads;
    // blocksPerGrid_y =  (N_y + threadsPerBlock_y - 1) / threadsPerBlock_y;
    blocksPerGrid_y = (N_y - 1) / threadsPerBlock_y + 1;

    DEBUGLN("Max Grid size = %d x %d \n", this->_props.maxGridSize[0],
            this->_props.maxGridSize[1]);
    DEBUGLN("All the calculationes have been done. (%d,%d)x(%d,%d) "
            "blocksPerGrid & threadsPerBlock. Kernel launch...\n",
            blocksPerGrid_x, threadsPerBlock_x, blocksPerGrid_y,
            threadsPerBlock_y);
  }

  // If the estimation was smaller than N then use N
  /*if (threadsPerBlock_x * blocksPerGrid_x * blocksPerGrid_y *
  threadsPerBlock_y < size)
  {
          DEBUGLN (" Adjusting to size %d \n",size);
          threadsPerBlock_x = 32;
          blocksPerGrid_x = max(N_x >> 5, 1);
          threadsPerBlock_y = 32;
          blocksPerGrid_y = max(N_y >> 5, 1);
  }*/

  // DEBUGLN ("%d\n", CUDA_ERROR_INVALID_VALUE);

  CHECK_ERROR(
      cuFuncSetBlockShape(func, threadsPerBlock_x, threadsPerBlock_y, 1));
  CHECK_ERROR(cuLaunchGrid(func, blocksPerGrid_x, blocksPerGrid_y));
  DEBUGLN("Kernel launch has finished. Synchronization needed\n");
  CHECK_ERROR(cuCtxSynchronize());
  DEBUGLN("Everything was OK.\n");
  
  Device::post_kernel_launch(region, name);

  std::vector<common::var_pair>::iterator iter;
  iter = vars.begin();
  while (iter != vars.end()) {
    h_var = iter->first;
    d_var = iter->second;
    common::Var *father = d_var->getParent();
    if (father) {
      d_var->copy_out(father);
      father->deleteChildren();
    }
    iter++;
  }

  return 0;
}

int CudaDevice::kernel_fixed_launch(common::Region *region, std::string name,
                                    tparams *params, int blocksPerGrid_x,
                                    int threadsPerBlock_x, int blocksPerGrid_y,
                                    int threadsPerBlock_y) {

  tparams *paramPtr = params;
  void *param;
  common::HostVar *h_var;
  common::Var *d_var;
  int offset = 0;
  std::vector<common::var_pair> vars;
  int size =
      blocksPerGrid_x * blocksPerGrid_y * threadsPerBlock_x * threadsPerBlock_y;
  size = abs(size);

  /* Loading the kernel function */
  CUfunction func;
  DEBUGLN("cuModuleGetFunction (_cuModule %d) - function: %s", _cuModule,
          name.c_str());
  std::map<std::string, CUmodule>::iterator it = _cuModules.find(name);
  if (it == _cuModules.end()) {
    DEBUGLN("Error! The %s function can not be found in the list of cuModules",
            name.c_str());
  } else {
    DEBUGLN("The %s function was found in the list of cuModules", name.c_str());
  }
  CHECK_ERROR(
      cuModuleGetFunction(&func, _cuModules.find(name)->second, name.c_str()));
  DEBUGLN("Kernel module file %s has been loaded", name.c_str());
  /* Getting the kernel parameteres */
  while (paramPtr) {
    if (paramPtr->type == common::SCALAR && strlen(paramPtr->reduction) == 0) {
      // Scalar variables
      if (paramPtr->size > sizeof(float))
        ALIGN_UP(offset, __alignof(double));
      else
        ALIGN_UP(offset, __alignof(float)); // Vale para enteros tambien
      CHECK_ERROR(
          cuParamSetv(func, offset, paramPtr->_base_ptr, paramPtr->size));
      offset += paramPtr->size;
    } else {
      param = paramPtr->_base_ptr;
      h_var = region->findHostVar(param);
      d_var = region->findDeviceVar(param);
      /* If the variable has not been initialised yet, let's copy it.
      */
      if (d_var->isEmpty() && !strlen(paramPtr->reduction) &&
          (h_var->getDirection() == common::IN ||
           h_var->getDirection() == common::INOUT)) {
        DEBUGLN(">>>>> Since d_var was empty, we copy host var to it \n");
        d_var->copy_in(h_var);
      } else if (!strlen(paramPtr->reduction)) {
        DEBUGLN(">>>>> d_var was not empty \n");
      } else {
        DEBUGLN(
            ">>>>> d_var was a reduction var and it is treated separately \n");
      }
      h_var->inUse();
      d_var->inUse();
      if (h_var->getDirection() == common::OUT ||
          h_var->getDirection() == common::INOUT) {
        d_var->dirty();
        common::Var *father = d_var->getParent();
        if (father)
          father->dirty();
      }
      if (strlen(paramPtr->reduction) != 0) {
        // Reduction variables
        CudaReductionVar *red_var = new CudaReductionVar();
        DEBUGLN("\n\nVariable %p has a new child %p\n", d_var, red_var);
        red_var->setParent(d_var);
        red_var->setDevice(this);
        red_var->setType(common::ARRAY);
        red_var->setSize(size * h_var->getSize());
        red_var->setCopyType(common::FORCE_COPY_IN);
        red_var->setBaseType(paramPtr->reductionType);
        red_var->setOperation(paramPtr->reduction);
        red_var->setIdentity(paramPtr->identity);
        red_var->alloc();
        d_var->addChild(red_var);
        vars.push_back(std::make_pair(h_var, red_var));
        CUdeviceptr *aux = (CUdeviceptr *)red_var->getPtr();
        ALIGN_UP(offset, __alignof(aux));
        CHECK_ERROR(cuParamSetv(func, offset, aux, sizeof(aux)));
        offset += sizeof(aux);
      } else {
        // Array variables
        d_var->setDevice(this);
        d_var->setSize(h_var->getSize());
        d_var->setType(paramPtr->type);
        CUdeviceptr *aux = (CUdeviceptr *)d_var->getPtr();
        ALIGN_UP(offset, __alignof(aux));
        CHECK_ERROR(cuParamSetv(func, offset, aux, sizeof(aux)));
        offset += sizeof(aux);
      }
    }
    paramPtr = paramPtr->next;
  } // while params
  CHECK_ERROR(cuParamSetSize(func, offset));

  // Increase cache size if possible
  if (computeCapability.first >= 2)
    CHECK_ERROR(cuFuncSetCacheConfig(func, CU_FUNC_CACHE_PREFER_L1));

  Device::pre_kernel_launch(region, name);
  if (blocksPerGrid_y < 0) {
    DEBUGLN("... Starting the launch of a kernel with one dimension\n");
    threadsPerBlock_x = min(threadsPerBlock_x, this->_props.maxThreadsPerBlock);
    CHECK_ERROR(cuFuncSetBlockShape(func, threadsPerBlock_x, 1, 1));
    CHECK_ERROR(cuLaunchGrid(func, blocksPerGrid_x, 1));
    DEBUGLN("All the calculationes have been done. ThreadsPerBlock: %d. "
            "BlocksPerGrid: %d. Kernel launch...\n",
            threadsPerBlock_x, blocksPerGrid_x);
  } else {
    DEBUGLN("... Starting the launch of a kernel with two dimensions\n");
    if (threadsPerBlock_x * threadsPerBlock_y > this->_props.maxThreadsPerBlock)
      throw CudaError();

    DEBUGLN("All the calculationes have been done. (%d,%d)x(%d,%d) "
            "blocksPerGrid & threadsPerBlock. Kernel launch...\n",
            blocksPerGrid_x, threadsPerBlock_x, blocksPerGrid_y,
            threadsPerBlock_y);
    CHECK_ERROR(
        cuFuncSetBlockShape(func, threadsPerBlock_x, threadsPerBlock_y, 1));
    CHECK_ERROR(cuLaunchGrid(func, blocksPerGrid_x, blocksPerGrid_y));
  }

  DEBUGLN("Kernel launch has finished. Synchronization needed\n");
  // We need this synchronization?
  CHECK_ERROR(cuCtxSynchronize());
  DEBUGLN("Everything was OK.\n");

  Device::post_kernel_launch(region, name);

  std::vector<common::var_pair>::iterator iter;
  iter = vars.begin();
  while (iter != vars.end()) {
    h_var = iter->first;
    d_var = iter->second;
    common::Var *father = d_var->getParent();
    if (father) {
      d_var->copy_out(father);
      father->deleteChildren();
    }
    iter++;
  }

  return 0;
}

void CudaDevice::kernelPreload(const common::TKernelInfo &kInfo, std::string PTXName) {
  _cuModule = loadPtx(PTXName + ".ptx");
  this->kInfoUpdate(kInfo.name, kInfo);
  this->_cuModules.insert(std::make_pair(kInfo.name, _cuModule));
  DEBUGLN("Kernel %s Preload OK", kInfo.name.c_str());
}

void CudaDevice::kernelPreload(std::string name) {
  _cuModule = loadPtx(name + ".ptx");
  this->_cuModules.insert(std::make_pair(name, _cuModule));
  common::TKernelInfo kInfo;
  kInfo.name = name;
  kInfo.numMemoryAccess = 10;
  kInfo.numFlop = 10;
  this->_kernelInfo.insert(std::make_pair(name, kInfo));
  DEBUGLN("Kernel %s Preload OK", name.c_str());
}

void CudaDevice::kernelPreload(const common::TKernelInfo &kInfo) {
  _cuModule = loadPtx(kInfo.name + ".ptx");
  this->kInfoUpdate(kInfo.name, kInfo);
  this->_cuModules.insert(std::make_pair(kInfo.name, _cuModule));
  DEBUGLN("Kernel %s Preload OK", kInfo.name.c_str());
}

void CudaDevice::wait(int event) {
  std::cout << "Wait is not implemented yet" << std::endl;
  exit(-1);
}

void CudaDevice::barrier() {
  CHECK_ERROR(cuCtxSynchronize());
  DEBUGLN("Sinchronization done\n");
}

CUmodule CudaDevice::loadPtx(std::string name) {
  // first search for the module path before we load the results
  CUmodule aux;
  std::string module_path, ptx_name, cubin_name, ptx_source;
  ptx_name = name; // The name that receives this function come whith the .ptx
  cubin_name = name + ".cubin";
  ptx_source = "";
  if (!CudaDevice::findModulePath(ptx_name.c_str(), module_path, name,
                                  ptx_source)) {
    if (!CudaDevice::findModulePath(cubin_name.c_str(), module_path, name,
                                    ptx_source)) {
      printf("> findModulePath could not find  ptx or cubin\n");
      //           Cleanup(false);
    }
  } else {
    DEBUGLN("> initCUDA loading module: <%s>\n", module_path.c_str());
  }

  // Create module from binary file (PTX or CUBIN)
  if (module_path.rfind("ptx") != std::string::npos) {
    // in this branch we use compilation with parameters
    const unsigned int jitNumOptions = 3;
    CUjit_option *jitOptions = new CUjit_option[jitNumOptions];
    void **jitOptVals = new void *[jitNumOptions];

    // set up size of compilation log buffer
    jitOptions[0] = CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES;
    int jitLogBufferSize = 2048;
    jitOptVals[0] = (void *)(size_t)jitLogBufferSize;

    // set up pointer to the compilation log buffer
    jitOptions[1] = CU_JIT_INFO_LOG_BUFFER;
    char *jitLogBuffer = new char[jitLogBufferSize];
    jitOptVals[1] = jitLogBuffer;

    // set up pointer to set the Maximum # of registers for a particular kernel
    jitOptions[2] = CU_JIT_MAX_REGISTERS;
    int jitRegCount = 32;
    jitOptVals[2] = (void *)(size_t)jitRegCount;

    CHECK_ERROR(cuModuleLoadDataEx(&aux, ptx_source.c_str(), jitNumOptions,
                                   jitOptions, (void **)jitOptVals));

    // std::cout << " Log: " << jitLogBuffer << std::endl;
    // std::cout << " Reg Count: " << jitRegCount << std::endl;

    return aux;
  } else {
    DEBUGLN("Error while loading PTX");
    CHECK_ERROR(cuModuleLoad(&aux, module_path.c_str()));
    return aux;
  }
  //    if (error != CUDA_SUCCESS) Cleanup(false);
}

common::Var *CudaDevice::createVar() {
  CudaVar *cvar = new CudaVar;
  cvar->setDevice(this);
  /** TODO: Check */
  return cvar;
}

void CudaDevice::stop() { CHECK_ERROR(cuCtxDestroy(_cuContext)); }

/*
*/
std::string findFile(std::string filename) {
  // search in data/
  if (filename.empty())
    return 0;

  size_t filename_len = strlen(filename.c_str());

  // Current dir as data folder
  const char data_folder[] = ".";
  size_t data_folder_len = strlen(data_folder);
  char *file_path =
      (char *)malloc(sizeof(char) * (data_folder_len + filename_len + 1));
  strcpy(file_path, data_folder);
  strcat(file_path, filename.c_str());
  size_t file_path_len = strlen(file_path);
  file_path[file_path_len] = '\0';
  std::fstream fh0(file_path, std::fstream::in);
  if (fh0.good())
    return file_path;
  free(file_path);

  return filename;
}

bool inline CudaDevice::findModulePath(const char *module_file,
                                       std::string &module_path,
                                       std::string moduleName,
                                       std::string &ptx_source) {

  module_path = findFile(module_file);
  if (module_path.empty()) {
    printf("> findModulePath could not find file: <%s> \n", module_file);
    return false;
  } else {
    DEBUGLN("> findModulePath found file at <%s>\n", module_path.c_str());

    if (module_path.rfind(".ptx") != std::string::npos) {
      FILE *fp = fopen(module_path.c_str(), "rb");
      if (!fp) {
        printf("*** PTX file %s not found\n", module_path.c_str());
        exit(-1);
      }
      fseek(fp, 0, SEEK_END);
      int file_size = ftell(fp);
      char *buf = new char[file_size + 1];
      fseek(fp, 0, SEEK_SET);
      fread(buf, sizeof(char), file_size, fp);
      fclose(fp);
      buf[file_size] = '\0';
      ptx_source = buf;
      delete[] buf;
    }
    return true;
  }
}

void CudaDevice::releaseAllPrograms() {
  DEBUGLN("releaseAllPrograms function has to be written");
}
}
