/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <cuda.h>
#include <frangollo_core/common/Device.h>
#include <frangollo_core/common/Var.h>
#include <frangollo_core/cuda/CudaDevice.h>
#include <frangollo_core/cuda/CudaReductionVar.h>
#include <math.h>
#include <stdio.h>

#define min(a, b) (a > b ? b : a)

#define CHECK_ERROR(whatever)                                                  \
  if ((whatever) != CUDA_SUCCESS) {                                            \
    printf("### (%s,%d) Error no was: %d #### \n", __FILE__, __LINE__,         \
           whatever);                                                          \
    throw CudaError();                                                         \
  }
#define DEBUG_ENABLE 0
namespace cuda {

template <typename T>
void reduction_func(CUdeviceptr *d_var, int numElems, CUmodule support,
                    char baseType, CUdeviceptr *father);

void CudaReductionVar::alloc() {
  Var::pre_alloc();
  //	 CUdeviceptr t = (this->getPtr<CUdeviceptr>());
  //   CHECK_ERROR(cuMemAlloc(&t, this->getSize()));
  // CUdeviceptr *new_var = (CUdeviceptr*) malloc (sizeof(CUdeviceptr));
  // this->setPtr(new_var);
  CUdeviceptr *t = (CUdeviceptr *)this->getPtr();
  CHECK_ERROR(cuMemAlloc(t, this->getSize()));
  Var::post_alloc();
}

void CudaReductionVar::free() {
  Var::pre_free();
  CUdeviceptr *p = (CUdeviceptr *)this->getPtr();
  CHECK_ERROR(cuMemFree(*p));
  Var::post_free();
}

void CudaReductionVar::copy_in(Var *var) {
  Var::pre_copy_in(var);
  // If var.device == this->device , local copy
  // If var.device != this->device and var.device.type = this.device.type,
  //    copy to other cuda device

  // If var.device != this->device and var.device.type != this.device.type
  //    then var.device.type *must* be host

  //  CUdeviceptr *d_var = (CUdeviceptr *)this->getPtr();

  //  int size = this->getSize()/32;
  /*if (this->getSize() % 32 != 0) {
          std::cerr << "--------------------- Error! The initialization of a
  cudaReductionVar is not properly done" << std::endl << std::endl;
          std::cerr << " Size no es divisible entre 32. Porque size es: " <<
  this->getSize() << std::endl;
  }
  cuMemsetD32 (*d_var, 0, size); */
  //   std::cout << "Copy_in de una variable de reduccion. Se meten 0 en un
  //   tamano de " << size*32 << std::endl;
  Var::post_copy_in(var);
}

void CudaReductionVar::copy_out(Var *var) {
  //   std::cout << std::endl << std::endl << "Se está realizando el copy_out de
  //   una variable de reducción!!!!!!! " << std::endl;
  Var::pre_copy_out(var);
  common::Device *cd = this->getDevice();
  CUmodule *support = (CUmodule *)cd->getSupportModule();
  int size = var->getSize();
  CUdeviceptr *d_var = (CUdeviceptr *)this->getPtr();
  CUdeviceptr *father_d_var = (CUdeviceptr *)var->getPtr();
  void *h_var = var->getPtr();
  //   std::cout << "--------------> size vale: " << size <<  ". Y getSize() "
  //   << this->getSize() << std::endl;
  int numElems = this->getSize() / size;
  CudaVar *father;
  father = (CudaVar *)var;

  if (this->getBaseType()[0] == 'd') {
    // std::cout<< "########### reduciendo para double" << std::endl;
    reduction_func<double>(d_var, numElems, *support, 'd', father_d_var);
    cuCtxSynchronize();
  } else if (this->getBaseType()[0] == 'f') {
    reduction_func<float>(d_var, numElems, *support, 'f', father_d_var);
    cuCtxSynchronize();
  } else if (this->getBaseType()[0] == 'i') {
    // std::cout<< "########### reduciendo para int" << std::endl;
    reduction_func<int>(d_var, numElems, *support, 'i', father_d_var);
    cuCtxSynchronize();
  } else
    std::cerr << "Error performing a reduction! Base type is "
              << this->getBaseType() << std::endl;

  this->clean();
  this->_parent->clean();
  this->_parent->deleteChild(this);
  /*   this->_parent->deleteChild(this);
     delete this;*/
  Var::post_copy_out(var);
}

unsigned int nextPow2(unsigned int x) {
  --x;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return ++x;
}

template <typename T>
void reduction_func(CUdeviceptr *d_var, int numElems, CUmodule support,
                    char baseType, CUdeviceptr *father) {
#define ALIGN_UP(offset, alignment)                                            \
  (offset) = ((offset) + (alignment)-1) & ~((alignment)-1)

  /* Num elems must be a power of two */
  double exponent = ceil(log2((double)numElems));
  /* reduction kernel asume potencia de dos */
  int n = (1 << (int)exponent);
  CUdeviceptr o_var;
  // std::cout << "\tEn reduction function" << std::endl;
  CHECK_ERROR(cuMemAlloc(&o_var, n * sizeof(T)));
  // T* reduction_loc_sum;
  // reduction_loc_sum = (T *) malloc(n*sizeof(T));

  int maxThreads = 512;
  int threads, blocks;
  if (DEBUG_ENABLE)
    std::cout << "\t# En reduction. Dvar tamaño: " << numElems
              << " y n en la funcion: " << n << std::endl;
  threads = (n < maxThreads * 2) ? nextPow2((n + 1) / 2) : maxThreads;
  printf("n= %d\n", n);
  printf("threads= %d\n", threads);
  // blocks = (n + (threads * 2 - 1)) / (threads * 2);
  blocks = (n - 1) / (threads * 2) + 1;
  int smemSize =
      (threads <= 32) ? 2 * threads * sizeof(T) : threads * sizeof(T);

  CUfunction reduction;
  if (baseType == 'd') {
    CHECK_ERROR(cuModuleGetFunction(&reduction, support, "reduction"));
  } else if (baseType == 'f') {
    CHECK_ERROR(cuModuleGetFunction(&reduction, support, "reduction_float"));
  } else {
    CHECK_ERROR(cuModuleGetFunction(&reduction, support, "reduction_int"));
  }
  int offset = 0;
  cuParamSetv(reduction, offset, d_var, sizeof(*d_var));
  offset += sizeof(*d_var);
  ALIGN_UP(offset, __alignof(o_var));
  cuParamSetv(reduction, offset, &o_var, sizeof(o_var));
  offset += sizeof(o_var);
  ALIGN_UP(offset, __alignof(n));
  cuParamSeti(reduction, offset, numElems);
  offset += sizeof(int);
  cuParamSetSize(reduction, offset);
  CHECK_ERROR(cuFuncSetSharedSize(reduction, smemSize));
  CHECK_ERROR(cuFuncSetBlockShape(reduction, threads, 1, 1));

  printf("Bloques= %d\n", blocks);
  CHECK_ERROR(cuLaunchGrid(reduction, blocks, 1));

  /* Final kernel*/
  cuCtxSynchronize();
  CUfunction finalReduction;
  if (baseType == 'd') {
    CHECK_ERROR(cuModuleGetFunction(&finalReduction, support, "final_double"));
  } else if (baseType == 'f') {
    CHECK_ERROR(cuModuleGetFunction(&finalReduction, support, "final_float"));
  } else {
    CHECK_ERROR(cuModuleGetFunction(&finalReduction, support, "final_int"));
  }
  offset = 0;
  cuParamSetv(finalReduction, offset, &o_var, sizeof(o_var));
  offset += sizeof(o_var);
  ALIGN_UP(offset, __alignof(father));
  cuParamSetv(finalReduction, offset, father, sizeof(*father));
  offset += sizeof(*father);
  ALIGN_UP(offset, __alignof(blocks));
  cuParamSeti(finalReduction, offset, blocks);
  offset += sizeof(int);
  cuParamSetSize(finalReduction, offset);
  CHECK_ERROR(cuFuncSetBlockShape(finalReduction, 1, 1, 1));

  CHECK_ERROR(cuLaunchGrid(finalReduction, 1, 1));
  cuCtxSynchronize(); /**/
}
}
