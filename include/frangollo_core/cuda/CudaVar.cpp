/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <frangollo_core/cuda/CudaReductionVar.h>
#include <frangollo_core/cuda/CudaVar.h>
// #define CHECK_ERROR(whatever) whatever

/* #define CHECK_ERROR(whatever) if((whatever) != CUDA_SUCCESS)
{ printf("### (%s,%d) Error no was: %d #### \n", __FILE__, __LINE__, whatever);
throw CudaError(); };
*/

namespace cuda {

void CudaVar::alloc() {
  // std::cout << "Alloc de una cuda var de tamaño " << this->getSize() <<
  // std::endl;
  Var::pre_alloc();
  CUdeviceptr *t = (CUdeviceptr *)this->getPtr();
  CHECK_ERROR(cuMemAlloc(t, this->getSize()));
  DEBUGLN(" Alloc var of size %d", this->getSize());
  Var::post_alloc();
}

void CudaVar::free() {
  // std::cout << "Free de una cuda var de tamaño " << this->getSize () <<
  // std::endl;
  Var::pre_free();
  CUdeviceptr *p = (CUdeviceptr *)this->getPtr();
  DEBUGLN(" DeAlloc var of size %d", this->getSize());
  CHECK_ERROR(cuMemFree(*p));
  Var::post_free();
}

void CudaVar::copy_in(Var *var) {
  if (Var::pre_copy_in(var) > 0) {
    if (this->hasChilds()) {
      //      VarPtrList
      std::vector<Var *>::iterator iter = _childs.begin();
      for (; iter != _childs.end(); iter++)
        (*iter)->copy_in(this);
    }

    CUdeviceptr *d_var = (CUdeviceptr *)this->getPtr();
    void *h_var = var->getPtr();

    CHECK_ERROR(cuMemcpyHtoD(*d_var, h_var, this->getSize()));
    DEBUGLN("Moving in var %p with size: %d \n", var->getPtr(),
            this->getSize());
  }
  Var::post_copy_in(var);
}

void CudaVar::copy_out(Var *var) {
  Var::pre_copy_out(var);
  if (this->hasChilds()) {
    std::cout << "Childs " << &_childs << std::endl;
    std::cout << this << " tiene hijos!!!! no deberia" << std::endl;
    std::vector<Var *>::iterator iter = _childs.begin();
    for (; iter != _childs.end(); iter++)
      (*iter)->copy_out(this);
  }

  CUdeviceptr *d_var = (CUdeviceptr *)this->getPtr();
  void *h_var = var->getPtr();
  CHECK_ERROR(cuMemcpyDtoH(h_var, *d_var, this->getSize()));
  DEBUGLN("Moving out var %p with size: %d \n", var->getPtr(), this->getSize());
  this->clean();
  Var::post_copy_out(var);
}
}