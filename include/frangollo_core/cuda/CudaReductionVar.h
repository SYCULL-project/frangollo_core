/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <cuda.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include <frangollo_core/common/Var.h>

#ifndef CUDA_REDUCTIONVAR_H
#define CUDA_REDUCTIONVAR_H

namespace cuda {

/**
* @brief This class is a special one to perform <b>reductions</b>. It is not
* visible for the programmer.
*/
class CudaReductionVar : public common::Var {
private:
  char *_baseType;
  char *_operation;
  char *_identity;

public:
  CudaReductionVar() {
    this->_ptr = (CUdeviceptr *)malloc(sizeof(CUdeviceptr));
    this->_baseType = 0;
  }

  void setBaseType(char *baseType) { this->_baseType = baseType; }
  char *getBaseType() { return _baseType; }
  void setOperation(char *operation) { this->_operation = operation; }
  char *getOperation() { return _operation; }
  void setIdentity(char *identity) { this->_identity = identity; }
  char *getIdentity() { return _identity; }

  void alloc();
  void free();
  /// Metodo que hace copy_in
  void copy_in(Var *var);
  /// @brief Metodo que hace copyout
  /// @param var Puntero a la host variable
  void copy_out(Var *var);
};
}
#endif
