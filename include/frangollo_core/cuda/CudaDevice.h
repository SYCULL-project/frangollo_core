/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cuda.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>

#include <frangollo_core/common/Device.h>
#include <frangollo_core/cuda/CudaReductionVar.h>
#include <frangollo_core/cuda/CudaVar.h>

#ifndef CUDA_DEVICE_H
#define CUDA_DEVICE_H

#define CHECK_ERROR(whatever)                                                  \
  if ((whatever) != CUDA_SUCCESS) {                                            \
    printf("### (%s,%d) Error no was: %d #### \n", __FILE__, __LINE__,         \
           whatever); /*throw CudaError();*/                                   \
  };

/**
 * @brief Namespace wich contains all the classes disegned for the
         NVIDIA CUDA architecture.
 */
namespace cuda {

/**
* @brief Special class to manage the CUDA errors.
*/
class CudaError : public std::exception {
  virtual const char *what() const throw() { return "Error with Cuda device"; }
};

const std::string CUDA_STRING = "CUDA";

/**
* @brief Manage the NVIDIA device.
*/
class CudaDevice : public common::Device {

private:
  /* Device pointers */
  CUdevice _cuDevice;
  CUcontext _cuContext;
  /* Device Code */
  CUmodule _cuModule, supportModule;
  std::map<std::string, CUmodule> _cuModules;
  /* Device identification / parameters */
  CUdevprop _props;
  //       std::pair<int,int> computeCapability;
  struct {
    int first;
    int second;
  } computeCapability;
  // Fixed number until 2.1 Compute Capability
  int blocksPerMP;
  inline unsigned warpsPerMP();
  inline unsigned regsPerMP();

protected:
  bool inline findModulePath(const char *module_file, std::string &module_path,
                             std::string moduleName, std::string &ptx_source);

  CUmodule loadPtx(std::string name);

  /* Estimate grid configuration */
  float computeOccupancy(const unsigned &nThreads, const unsigned &nRegs,
                         const unsigned &shMem);

  unsigned maximizeOccupancy(const unsigned &nThreads, const unsigned &nRegs,
                             const unsigned &shMem);

  unsigned estimateNumThreads(const unsigned &numMemAccess,
                              const unsigned &numFlop, const unsigned &numRegs);

public:
  CudaDevice() {
    this->_id = 1;
    this->_isOnline = false;
    blocksPerMP = 8;
    this->_threadNumber = -1;
    this->_device_string = CUDA_STRING;
  };

  /* Init library / device  */
  void init();

  void *getSupportModule() { return &supportModule; };
  void setSupportModule(CUmodule *module) { supportModule = *module; }
  void *getType() {
    return NULL;
  } // this method has no sense in cuda. Only in OpenCL (gpu or cpu)

  /* Preloads kernels from a given module name */
  void kernelPreload(std::string name);
  void kernelPreload(const common::TKernelInfo &kInfo, std::string PTXName);
  void kernelPreload(const common::TKernelInfo &kInfo);
  void releaseAllPrograms();
  void barrier();
  void wait(int event);

  /* Launch a kernel from a given module name */
  int kernel_launch(common::Region *region, std::string name, tparams *params,
                    int size_x, int size_y, int size_z);
  /*int kernel_launch2D(common::Region *region, std::string name,
                       tparams * params, int size1, int size2);*/

  /* Launch a fixed kernel from a given module name */
  int kernel_fixed_launch(common::Region *region, std::string name,
                          tparams *params, int blocksPerGrid_x,
                          int threadsPerBlock_x, int blocksPerGrid_y,
                          int threadsPerBlock_y);

  // int isBigEnough(common::Region * region, std::string name, int size);

  /* Deactivate device */
  void stop();

  common::Var *createVar();
};
}
#endif
