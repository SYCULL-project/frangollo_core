/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <config.h>
#include <frangollo_core/System.h>

#if HAVE_LIBCUDA
#include <frangollo_core/cuda/CudaDevice.h>
#endif

#if HAVE_LIBOCL
#include <frangollo_core/ocl/OCLDevice.h>
#endif

#if NATIVE_SMP_ENABLE
#include <frangollo_core/smp/SMPDevice.h>
#endif

#define DEBUG_ENABLE 0

namespace common {

System system;

template <class T> T fromString(const std::string &s) {
  std::istringstream stream(s);
  T t;
  stream >> t;
  return t;
}

System::~System() {
  if (this->_started == true) {
    std::vector<Device *>::iterator it;
    for (it = _deviceList.begin(); it != _deviceList.end(); it++) {
      (*it)->stop();
    }
    // End_Trace(); allowed if tracing installed
    this->_started = false;
  }
}

System::System() {
  this->_started = false;
  // #ifdef AUTOMATIC_INITIALIZATION
  init();
  // #endif
}

void System::init() {
  if (this->_started) {
    /* Just to avoid re-initialisation */
    return;
  }
  /* Recognition of available devices */
  // Start_Trace(); allowed if tracing installed
  this->cudaPos = this->oclPos = -1;
  this->env[FRANGOLLO_GRANULARITY_FACTOR] =
      getenv("FRANGOLLO_GRANULARITY_FACTOR");

  this->env[ACC_DEVICE] = getenv("ACC_DEVICE");
  this->env[FRANGOLLO_PLATFORM] = getenv("FRANGOLLO_PLATFORM");
  this->env[FRANGOLLO_THREAD_NUM] = getenv("FRANGOLLO_THREAD_NUM");
  this->env[ACC_DEVICE_NUM] = getenv("ACC_DEVICE_NUM");

#ifdef HAVE_LIBCUDA
  this->env[FRANGOLLO_THREAD_NUM] = getenv("FRANGOLLO_THREAD_NUM");
  this->_deviceList.push_back(new cuda::CudaDevice());
  this->cudaPos = 0;
  if (this->env[FRANGOLLO_GRANULARITY_FACTOR])
    this->_deviceList[0]->setGranularityFactor(
        fromString<float>(this->env[FRANGOLLO_GRANULARITY_FACTOR]));
  else
    this->_deviceList[0]->setGranularityFactor(3.0f);

  if (this->env[FRANGOLLO_THREAD_NUM]) {
    this->_deviceList[0]->setThreadNumber(
        fromString<int>(this->env[FRANGOLLO_THREAD_NUM]));
  } else
    this->_deviceList[0]->setThreadNumber(-1);
#endif

#ifdef HAVE_LIBOCL
  this->_deviceList.push_back(new ocl::OCLDevice());
  this->oclPos = this->cudaPos + 1;
#endif

#ifdef NATIVE_SMP_ENABLE
  this->_deviceList.push_back(new smp::SMPDevice());
#endif

  if (_deviceList.size() == 0) {
    std::cerr << " No device was available on the system " << std::endl;
    // if (DEBUG_ENABLE) std::clog << "Error! NO device was available on the
    // system" << std::endl;
  }
  /* Initialize available devices */
  std::vector<Device *>::iterator it;

  for (it = _deviceList.begin(); it != _deviceList.end(); it++) {
    (*it)->init();
  }
  this->_started = true;
  ;
}

Device *System::getDevice() {
  if (DEBUG_ENABLE)
    std::clog << " Get device " << std::endl;

  if (!this->_started)
    init();

  std::string franPlatform;
  char *franPlatform_char;
  franPlatform_char = this->env[FRANGOLLO_PLATFORM];
  if (franPlatform_char != NULL) {
    franPlatform = franPlatform_char;
    if (franPlatform.size() != 0 && DEBUG_ENABLE)
      std::clog << " Current Frangollo Platform is " << franPlatform
                << std::endl;

    if (franPlatform == "CUDA" && cudaPos >= 0) {
      return this->_deviceList[cudaPos];
    }

    if (franPlatform == "OPENCL" && this->oclPos >= 0) {
      return this->_deviceList[this->oclPos];
    }
  }
  if (DEBUG_ENABLE)
    std::clog << " Default return " << std::endl;
  /* Cheap solution, always returns first */
  return this->_deviceList[0];
}

std::vector<Device *> System::getDeviceList() { return this->_deviceList; }
}
