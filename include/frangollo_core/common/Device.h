/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <exception>
#include <iostream>
#include <set>
#include <string>

#ifndef DEVICE_H
#define DEVICE_H

typedef struct _tparams {
  void *_base_ptr;
  int type;
  char *reduction;
  char *reductionType;
  char *identity;
  unsigned int canChange;
  size_t size;
  char *varName;
  struct _tparams *next;
} tparams;

#define MAX_KERNEL_NAME 255
typedef struct {
  char kernel_name[MAX_KERNEL_NAME];
  /* Information about kernel to help guessing
          the proper thread/block combination */
  unsigned int numMemoryAccess;
  unsigned int numFlop;
} tkernel_list;

/**
 * @brief This namespace contains all the clases that are not designed for a
 * specific architecture.
 *
 */
namespace common {
class Device;
class KernelLaunchError : public std::exception {
private:
  short int _id;
  std::string _s;

public:
  KernelLaunchError(std::string s, short int id) : _id(id), _s(s){};
  ~KernelLaunchError() throw(){};

  virtual const char *what() const throw() {
    return "Error while trying to launch a Kernel on the Device";
  }
};
}

#include <frangollo_core/common/Region.h>
#include <frangollo_core/common/Var.h>
#include <frangollo_core/common/debugmacros.h>

namespace common {

typedef struct {
  std::string name;
  /* Information about kernel to help guessing
          the proper thread/block combination */
  unsigned int numMemoryAccess;
  unsigned int numFlop;
} TKernelInfo;

#include <time.h>

/**
* @brief Device Statistics
*/
class DeviceStats {
protected:
  long int _in_bytes;
  long int _out_bytes;
  long int _current;

  /* Timers */
  typedef struct timespec CLOCK_TYPE;
  double _transfer_time_in, _transfer_time_out;
  double _in_kernel_time;
  CLOCK_TYPE _transfer_clock_start, _transfer_clock_end;
  CLOCK_TYPE _transfer_in_clock_start, _transfer_in_clock_end;
  CLOCK_TYPE _kernel_clock_start, _kernel_clock_end;

  inline void chrono_gettime(CLOCK_TYPE &tv);
  inline double diftime(common::DeviceStats::CLOCK_TYPE ch,
                        common::DeviceStats::CLOCK_TYPE ch2);

public:
  DeviceStats() {
    _in_bytes = 0;
    _out_bytes = 0;
    _current = 0;
    _transfer_time_in = _transfer_time_out = 0;
    _in_kernel_time = 0;
  };
  long int get_transfer_in() { return _in_bytes; };
  long int get_transfer_out() { return _out_bytes; };
  long int get_current() { return _current; };
  void startTransferIn();
  void finishTransferIn();
  void startTransferOut();
  void finishTransferOut();
  double getTransferInTime() { return _transfer_time_in; }
  double getTransferOutTime() { return _transfer_time_out; }
  void allocate(long int alloc_bytes) { _current += alloc_bytes; };
  void deallocate(long int dealloc_bytes) { _current -= dealloc_bytes; };
  void addIn(long int in_bytes) {
    _in_bytes += in_bytes;
    ;
  }
  void addOut(long int out_bytes) { _out_bytes += out_bytes; };

  /* Kernel information */
  void startKernel();
  void stopKernel();
  double getKernelTime() { return _in_kernel_time; };

  friend std::ostream &operator<<(std::ostream &out, DeviceStats *stats);
};

/**
* @brief An abstract class for a generic device.
*/
class Device {

private:
protected:
  int _id;
  bool _isOnline;
  float _granularityFactor;
  int _threadNumber;
  std::string _device_string;
  /* Variable residing on device */
  std::set<Device *> dvars;
  /* Information about kernels loaded in the device */
  std::map<std::string, common::TKernelInfo> _kernelInfo;
  // Hardware device information
  std::string _hwdevice;

  void registerVar(Var *var) {}

  DeviceStats *_stats;

public:
  Device() {
    _device_string = "None";
    _stats = new DeviceStats();
  };
  virtual void init() = 0;
  virtual void stop() = 0;

  void setGranularityFactor(const float &factor) {
    _granularityFactor = factor;
  }
  float getGranularityFactor() { return _granularityFactor; }

  void setThreadNumber(const int &thn) { _threadNumber = thn; }
  int getThreadNumber() { return _threadNumber; }

  virtual void *getType() = 0;
  virtual void barrier() = 0;
  virtual void wait(int event) = 0;

  /* Preload kernels from a given region */
  virtual void kernelPreload(std::string name) = 0;
  virtual void kernelPreload(const common::TKernelInfo &kInfo) = 0;
  virtual void kernelPreload(const common::TKernelInfo &kInfo, std::string PTXName) = 0;
  virtual void *getSupportModule() = 0;

  /* Kernel launch */
  void pre_kernel_launch(Region *region, std::string name);
  void post_kernel_launch(Region *region, std::string name);

  virtual int kernel_launch(Region *region, std::string name, tparams *params,
                            int size_x, int size_y, int size_z) = 0;

  virtual int kernel_fixed_launch(common::Region *region, std::string name,
                                  tparams *params, int blocksPerGrid_x,
                                  int threadsPerBlock_x, int blocksPerGrid_y,
                                  int threadsPerBlock_y) = 0;

  /* Public interface for create a var within the device */
  virtual Var *createVar() = 0;

  /* Update kernel info */
  void kInfoUpdate(const std::string &name, const TKernelInfo &kInfo) {
    this->_kernelInfo.insert(std::make_pair(kInfo.name, kInfo));
  }

  /* Device information */
  std::string getDeviceString() { return _device_string; };
  std::string getHWDeviceString() { return _hwdevice; };
  DeviceStats *getDeviceStats() { return _stats; };
  void showStats();
  virtual void releaseAllPrograms() = 0;
};
}

#endif
