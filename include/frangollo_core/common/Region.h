/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <frangollo_core/System.h>
#include <frangollo_core/common/Device.h>
#include <frangollo_core/common/Var.h>
#include <iostream>
#include <map>
#include <vector>

#ifdef __DEBUG__
#include <stdio.h>
#endif
#include <frangollo_core/common/debugmacros.h>

#ifndef REGION_H
#define REGION_H

namespace common {

extern System system;

/**
* @brief Manage a llc region.
*/
class Region {

private:
  /// @todo This needs to be generic
  typedef std::map<void *, HostVar *> CorrMap;
  typedef std::map<void *, Var *> DeviceMap;
  typedef std::map<HostVar *, Var *> HtD_Map;
  typedef std::pair<HostVar *, Var *> HtD_pair;
  typedef std::set<std::string> Kernel_list;

  typedef std::set<void *> ScopeType;

  std::string _name;
  Device *_dev;

  std::vector<HostVar *> hostVars;

  CorrMap _currentMap;
  HtD_Map _deviceMap;
  DeviceMap _deviceVars;
  int __nestedCall;
  Kernel_list _kernel_list;

  std::vector<ScopeType *> _scope;

protected:
  /* Register a Var from the Region Devices
            into the context */
  void registerDeviceVar(Var *var);
  /* Map var to device */
  void updateHostVar(HostVar *hostVar);

public:
  Region() : _dev(NULL) { __nestedCall = 1; };
  ~Region();

  void destroy();

  Region(std::string name);
  Region(std::string name, Device *dev);

  void setName(std::string name);
  std::string getName() { return _name; };

  int belongs_scope(void *var);

  /*** Device Methods **/
  /* Current model only allows one device*/
  Device *getDevice();
  // Device * getDevice(DKind k);
  void setDevice(Device *dev);
  /************** Variable Correspondence handling */

  void addHostVar(HostVar *var);
  HostVar *findHostVar(void *var);
  void removeVar(void *var);

#if 0
    void addDeviceVar(void *ptr, size_t size, var_layout_t var_layout,
                      var_layout_t &var_layout_p, int direction, int canChange,
                      int copyType);
#endif
  void addDeviceVar(void *ptr, size_t size, var_layout_t var_layout);

  Var *findDeviceVar(void *var);
  int isPointerInside(void *var);

  /************** Reconcyle Operations */
  /* Reconcyle all current dirty variables */
  void concyleAll();
  /* Concyle a variable ptr accorss al context devices */
  void concyleVar(void *var);
  /* Concyle a particular host var */
  void concyleHostVar(HostVar *hvar);

  /****************** Scope Management */
  void incNested();
  void decNested();
  int getNestedCalls() { return __nestedCall; }

  /****************** Add a kernel to context devices ***/
  int addKernel(std::string name);
  int searchKernel(std::string name);
};
}

#endif
