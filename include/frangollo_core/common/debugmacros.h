/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#ifndef DEBUG_MACROS_H
#ifndef __DEBUG__
#define __DEBUG__ 0
#endif

#include <stdio.h>

#define DEBUG(...)                                                             \
  do {                                                                         \
    if (__DEBUG__) {                                                           \
      printf("(%s,%d)", __FILE__, __LINE__);                                   \
      printf(__VA_ARGS__);                                                     \
    }                                                                          \
  } while (0);
#define DEBUGLN(...)                                                           \
  do {                                                                         \
    if (__DEBUG__) {                                                           \
      printf("(%s,%d)", __FILE__, __LINE__);                                   \
      printf(__VA_ARGS__);                                                     \
      printf("\n");                                                            \
      fflush(stdout);                                                          \
    }                                                                          \
  } while (0);
#endif
