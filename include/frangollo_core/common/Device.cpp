/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <frangollo_core/common/Device.h>

#include <iostream>

std::ostream &operator<<(std::ostream &out, common::DeviceStats *stats) {
  out << "(In : " << (unsigned int)stats->get_transfer_in()
      << " out : " << (unsigned int)stats->get_transfer_out() << " current "
      << (unsigned int)stats->get_current() << " )" << std::endl;
  return out;
}

void common::Device::showStats() {
  std::cout << " FRANGOLLO STATS " << std::endl;
  std::cout << " Statistics for device " << std::endl;
  std::cout << "(In : " << this->_stats->get_transfer_in()
            << " Out : " << this->_stats->get_transfer_out()
            << " current: " << this->_stats->get_current();
  std::cout << " Time transf. in " << (this->_stats->getTransferInTime())
            << "s ";
  std::cout << " Time transf. out " << (this->_stats->getTransferOutTime())
            << "s ";
  std::cout << " Time kernel " << (this->_stats->getKernelTime()) << "s )"
            << std::endl;
}

#ifdef __MACH__
#define CLOCK_MONOTONIC 1
#include <sys/time.h>
// clock_gettime is not implemented on OSX
int clock_gettime(int /*clk_id*/, struct timespec *t) {
  struct timeval now;
  int rv = gettimeofday(&now, NULL);
  if (rv)
    return rv;
  t->tv_sec = now.tv_sec;
  t->tv_nsec = now.tv_usec * 1000;
  return 0;
}
#endif

inline void
common::DeviceStats::chrono_gettime(common::DeviceStats::CLOCK_TYPE &tv) {
  clock_gettime(CLOCK_MONOTONIC, &tv);
}

inline double
common::DeviceStats::diftime(common::DeviceStats::CLOCK_TYPE ch,
                             common::DeviceStats::CLOCK_TYPE ch2) {
  timespec temp;
  if ((ch2.tv_nsec - ch.tv_nsec) < 0) {
    temp.tv_sec = ch2.tv_sec - ch.tv_sec - 1;
    temp.tv_nsec = 1000000000 + ch2.tv_nsec - ch.tv_nsec;
  } else {
    temp.tv_sec = ch2.tv_sec - ch.tv_sec;
    temp.tv_nsec = ch2.tv_nsec - ch.tv_nsec;
  }
  double accum = (temp.tv_sec + ((double)(temp.tv_nsec)) * 1e-9f);
  return accum;
}

void common::DeviceStats::startTransferIn() {
  this->chrono_gettime(_transfer_in_clock_start);
}

void common::DeviceStats::finishTransferIn() {
  this->chrono_gettime(_transfer_in_clock_end);
  double tmp_time =
      diftime(this->_transfer_in_clock_start, this->_transfer_in_clock_end);
  _transfer_time_in += tmp_time;
}

void common::DeviceStats::startTransferOut() {
  this->chrono_gettime(_transfer_clock_start);
}

void common::DeviceStats::finishTransferOut() {
  this->chrono_gettime(_transfer_clock_end);
  double tmp_time =
      diftime(this->_transfer_clock_start, this->_transfer_clock_end);
  _transfer_time_out += tmp_time;
}

void common::DeviceStats::startKernel() {
  this->chrono_gettime(_kernel_clock_start);
}

void common::DeviceStats::stopKernel() {
  this->chrono_gettime(_kernel_clock_end);
  double tmp_time = diftime(this->_kernel_clock_start, this->_kernel_clock_end);
  _in_kernel_time += tmp_time;
}

void common::Device::pre_kernel_launch(Region *region, std::string name) {
  this->_stats->startKernel();
}

void common::Device::post_kernel_launch(Region *region, std::string name) {
  this->_stats->stopKernel();
}
