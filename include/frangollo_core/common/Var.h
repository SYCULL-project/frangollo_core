/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <frangollo_core/common/Device.h>
#include <iostream>
#include <vector>

#ifndef VAR_H
#define VAR_H

enum VarStatus { EMPTY, COPYIN, READY, INUSE, COPYOUT };
#ifndef VAR_LAYOUT
#define VAR_LAYOUT
typedef struct {
  int VarDirection;
  int CopyType;
  int VarType;
  int CanChange;
  size_t x, y;
  size_t lda;
  size_t pitch;
  size_t elemSize;
} var_layout_t;
#endif

namespace common {

/**
* @brief Enumeration to indicate the direction of a variable.
*/
enum VarDirection { IN, OUT, INOUT, NONE };

enum CopyType { FORCE_COPY_IN, COPY_FIRST_ONLY };

/**
* @brief Enumeration to indicate the type of a variable. It is necessary to
* perform correctly the cuParamSetv calls.
*/
enum VarType { SCALAR, ARRAY, ARRAY2D };

/**
* @brief Potential variable status
*
*    EMPTY : Variable has been allocated but no transfer have been made
*    COPYIN : Variable is being transferred in
*    COPYOUT : Variable is being transferred out
*    READY : A variable is ready to use, with data in it
*    INUSE : A kernel is using the variable
*/

/**
* @brief Variable equivalence (HostVar to any var)
*/
class Var;
class HostVar;
typedef std::pair<common::HostVar *, common::Var *> var_pair;
typedef std::vector<Var *> VarPtrList;

/**
* @brief An abstrac class for a generic variable.
*/
class Var {

private:
  int _varId;
  size_t _size;
  VarDirection _dir; /* Var direction related to CONTEXT */
  var_layout_t info2D;
  bool _canChange; /* Variable can be modified inside context */
  bool _dirty;     // Variable contents have been changed since last copyin
  bool _empty;     // Check if var have been initialized  or not
  CopyType _copyType;
  int _type;

protected:
  void *_ptr;
  VarStatus _status; // Current status of the variable
  Device *_device;
  void *_hostPtr; // Pointer to a memory position in the host
                  //  some components, like OpenCL Var, use this ptr to
                  //  improve performance
  /* Variable hierarchy */
  VarPtrList _childs; /* Other runtime instances of this variable */
  Var *_parent;       /* Parent of this variable instance */

public:
  Var() {
    _type = 0;
    _dirty = false, _status = EMPTY;
    _parent = NULL;
    _canChange = true;
    _hostPtr = NULL;
  }
  Var(void *var, size_t size, VarDirection varDirection, Device *device);
  void setDevice(Device *device) { _device = device; }

  void setType(int type) { _type = type; }
  /* Get Frangollo var type (not real type) */
  int getType() { return _type; }

  /*********** Variable device **/
  /* Returns a ptr to the Frangollo device handler */
  Device *getDevice() { return _device; }

  /*********** Size ***/
  void setSize(size_t size) { _size = size; }
  size_t getSize() { return _size; }
  void set2DInfo(var_layout_t layout) { info2D = layout; }
  var_layout_t get2DInfo() { return info2D; }

  /*********** Directionality ***/
  void setDirection(int varDirection) { _dir = (VarDirection)varDirection; }
  VarDirection getDirection() { return _dir; }

  /*********** Status ***/
  bool isDirty() { return _dirty; };
  void dirty() { _dirty = true; }; // It has been modified since last copyin
  void clean() {
    _dirty = false;
  }; // It is "concyled" (no changes since last copyin)

  // Status
  int getStatus() { return _status; };
  bool isEmpty() { return _status == EMPTY; };
  void markEmpty() { _status = EMPTY; };
  void notEmpty() { _status = READY; };
  void inUse() { _status = INUSE; };
  /* A variable is usable iff is ready or empty. Otherwhise, there is an
     operation going
        on and we must wait */
  bool isUsable() { return _status == READY || _status == EMPTY; }

  /* Information about if the variable can
     be changed inside context or not */
  bool canChange() { return _canChange; }
  void set_writable() { _canChange = true; }
  void set_readOnly() { _canChange = false; }

  void setCopyType(int copyType) { _copyType = (CopyType)copyType; }
  int getCopyType() { return _copyType; }

  /********** Childs **/
  void addChild(Var *child);
  void deleteChild(Var *child);
  void deleteChildren();
  bool hasChilds();
  int getNumChildren();

  void setParent(Var *parent);
  Var *getParent() { return _parent; }

  bool isOverrride();

  /*********** Access to variable pointer ***/
  void setPtr(void *ptr) { _ptr = ptr; }
  void *getPtr() { return _ptr; }
  /*********** Access to a host memory position if any ***/
  void *getHostPtr() { return _hostPtr; };
  void setHostPtr(void *hostPtr) { _hostPtr = hostPtr; };

  /*** General actions ***/
  int pre_copy_in(Var *var);
  void post_copy_in(Var *var);
  void pre_copy_out(Var *var);
  void post_copy_out(Var *var);
  void pre_alloc();
  void post_alloc();
  void pre_free();
  void post_free();

  /*********** Child specific methods ****/
  virtual void alloc() = 0;
  virtual void free() = 0;
  virtual void copy_in(Var *var) = 0;
  virtual void copy_out(Var *var) = 0;
};

/**
* @brief Manage a data storaged in the host.
*/
class HostVar : public Var {

public:
  HostVar() { this->_ptr = NULL; };
  HostVar(void *var, size_t size, VarDirection varDirection);
  // ~HostVar();
  void alloc();
  void free();
  void copy_in(Var *var);
  void copy_out(Var *var);
};
};

#endif
