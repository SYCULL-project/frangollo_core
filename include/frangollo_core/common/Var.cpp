/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <frangollo_core/common/Var.h>

#include <stdio.h>

namespace common {

Var::Var(void *var, size_t size, VarDirection varDirection, Device *device) {
  this->setSize(size);
  this->setPtr(var);
  this->setDirection(varDirection);
  this->markEmpty();
  this->setDevice(device);
}

void Var::addChild(Var *child) { this->_childs.insert(_childs.begin(), child); }

void Var::deleteChildren() { _childs.clear(); }

int Var::getNumChildren() { return _childs.size(); }

void Var::deleteChild(Var *child) {
  std::vector<Var *>::iterator it = _childs.begin();
  int i = 0;
  bool deleted = false;
  while (it != _childs.end()) {
    if (child == _childs[i]) {
      _childs.erase(it);
      deleted = true;
      break;
    } else {
      i++;
      it++;
    }
  }
  if (!deleted)
    std::cout << "Error while erasing a child from a father" << std::endl;
}

bool Var::hasChilds() { return (this->_childs.size() > 0); }

/* @brief Method called before any copy_in operation */
int Var::pre_copy_in(Var *var) {
  if (this->getCopyType() == common::FORCE_COPY_IN ||
      (this->getCopyType() == common::COPY_FIRST_ONLY && this->isEmpty())) {
    // std::cout << "precopy se permite el copy de una variable de size " <<
    // this->getSize() << " de tipo " << this->getCopyType() << " y si esta
    // vacia o no ahora te lo digo: " << this->getStatus() << std::endl;
    this->_status = COPYIN;
    Device *dev = this->getDevice();
    dev->getDeviceStats()->startTransferIn();
    return 1;
  } else {
    return 0;
  }
}

/* @brief Method called after any copy_in operation */
void Var::post_copy_in(Var *var) {
  // After any copy_in operation , variable has a value
  this->_status = READY;
  this->_dirty = false;
  if (this->getCopyType() == common::FORCE_COPY_IN ||
      (this->getCopyType() == common::COPY_FIRST_ONLY && this->isEmpty())) {
    Device *dev = this->getDevice();
    dev->getDeviceStats()->finishTransferIn();
    dev->getDeviceStats()->addIn(var->getSize());
  }
}

void Var::pre_copy_out(Var *var) {
  this->_status = COPYOUT;
  Device *dev = this->getDevice();
  dev->getDeviceStats()->startTransferOut();
}

void Var::post_copy_out(Var *var) {
  this->_status = READY;
  Device *dev = this->getDevice();
  dev->getDeviceStats()->finishTransferOut();
  dev->getDeviceStats()->addOut(var->getSize());
}

void Var::pre_alloc() { this->markEmpty(); };

void Var::post_alloc() {
  Device *dev = this->getDevice();
  dev->getDeviceStats()->allocate(this->getSize());
};

void Var::pre_free(){

};

void Var::post_free() {
  Device *dev = this->getDevice();
  dev->getDeviceStats()->deallocate(this->getSize());
};

/***** Deprecated ****/
void Var::setParent(Var *parent) { this->_parent = parent; }

/**** Deprecated ****/
bool Var::isOverrride() { return ((this->_parent != NULL)); }

HostVar::HostVar(void *var, size_t size, VarDirection varDirection) {
  this->setSize(size);
  this->setPtr(var);
  this->setDirection(varDirection);
}

void HostVar::alloc() {}

void HostVar::free() {}

void HostVar::copy_in(Var *var) {}

void HostVar::copy_out(Var *var) {}
}
