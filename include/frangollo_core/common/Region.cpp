/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <assert.h>
#include <frangollo_core/common/Region.h>
#include <frangollo_core/common/Var.h>

namespace common {

Region::Region(std::string name, Device *dev) {
  __nestedCall = 1;
  this->setName(name);
  this->setDevice(dev);
}

Region::Region(std::string name) {
  __nestedCall = 1;
  DEBUGLN("Common Region init ");
  Device *dev = system.getDevice();
  DEBUGLN("Device is %p \n", dev);
  this->setName(name);
  this->setDevice(dev);
  ScopeType *st = new ScopeType;
  _scope.push_back(st);
  DEBUGLN("Common Region OK ");
}

void Region::setName(std::string name) { _name = name; }

Device *Region::getDevice() { return _dev; }

void Region::setDevice(Device *dev) { _dev = dev; }

Region::~Region() {
  // destroy();
}

void Region::destroy() {
  _currentMap.clear();
  _deviceMap.clear();
  DeviceMap::iterator it = _deviceVars.begin();
  for (it; it != _deviceVars.end(); it++)
    it->second->free();
  _deviceVars.clear();
  hostVars.clear();
  _kernel_list.clear();

  this->getDevice()->barrier();
  DEBUGLN(" Region Finished\n");
}

#if 0
void Region::addDeviceVar(void *ptr, size_t size, var_layout_t var_layout,
                          var_layout_t &var_layout_p, int direction,
                          int canChange, int copyType) {
#endif
void Region::addDeviceVar(void *ptr, size_t size, var_layout_t var_layout) {
  DeviceMap::iterator it = _deviceVars.find(ptr);
  if (it == _deviceVars.end()) {
    // Variable has not been registered before
    std::pair<void *, Var *> correlation;
    correlation.first = ptr;
    Var *dVar;
    dVar = _dev->createVar();
    // ptr, size, direction);
    // dVar->setPtr (ptr);
    dVar->setDirection(var_layout.VarDirection);
    dVar->setSize(size);
    dVar->setType(var_layout.VarType);
    if (var_layout.VarType == common::ARRAY2D)
      dVar->set2DInfo(var_layout);
    dVar->setCopyType(var_layout.CopyType);
    dVar->setDevice(this->getDevice());
    if (var_layout.CanChange) {
      dVar->set_writable();
    } else {
      dVar->set_readOnly();
    }
    dVar->setHostPtr(ptr);
    dVar->alloc();
#if 0
      if (var_layout.type == common::ARRAY2D)
        var_layout_p = dVar->get2DInfo();
#endif
    correlation.second = dVar;
    _deviceVars.insert(correlation);

    DEBUGLN("New device var (%p) correlated to host var %p \n", dVar,
            ptr); // << std::endl;
    HtD_pair var_pair;
    HostVar *hVar = (_currentMap.find(ptr))->second;
    var_pair.first = hVar;
    var_pair.second = dVar;
    _deviceMap.insert(var_pair);
    DEBUGLN("New device var (%p) correlated to HostVar %p \n", dVar,
            hVar); // << std::endl;
    // Scope
    DEBUGLN("# En el registerVar. El scope actual es %d", __nestedCall - 1);
    if (var_layout.VarDirection == common::OUT ||
        var_layout.VarDirection == common::INOUT) {
      ScopeType *list = _scope[__nestedCall - 1];
      list->insert(ptr);
      DEBUGLN("# Ahora, en este nivel hay %d variables OUT o INOUT",
              (int)list->size());
    }
    // Prefetching is only valid on the uppermost scope
    DEBUGLN(
        "# Checking now if var %p should be copied in, direction %d nested %d ",
        hVar->getPtr(), var_layout.VarDirection, __nestedCall);
    if ((var_layout.VarDirection == common::IN ||
         var_layout.VarDirection == common::INOUT) &&
        ((__nestedCall - 1) == 0)) {
      DEBUGLN("# Copying %p ", hVar->getPtr());
      dVar->copy_in(hVar);
    } else {
      DEBUGLN("# Not copying  %p , level %d ", hVar->getPtr(), __nestedCall);
    }
  } // End if not registered before
}

Var *Region::findDeviceVar(void *var) {
  DeviceMap::iterator iter;
  iter = _deviceVars.find(var);
  if (iter == _deviceVars.end()) {
    // TODO: handle errors
    std::cerr << "ERROR: Device equivalence to " << var
              << " could not be found. " << std::endl;
    throw std::bad_exception();
    return NULL;
  } else {
    // DEBUGLN(" OK");
    return iter->second;
  }
}

int Region::isPointerInside(void *var) {
  CorrMap::iterator it;
  it = _currentMap.find(var);
  if (it == _currentMap.end())
    return -1;
  else
    return 1;
}

void Region::addHostVar(HostVar *var) {
  CorrMap::iterator it = _currentMap.find(var->getPtr());
  if (it == _currentMap.end()) {
    hostVars.push_back(var);
    std::pair<void *, HostVar *> correlation;
    correlation.first = var->getPtr();
    correlation.second = var;
    _currentMap.insert(correlation);
  }
}

HostVar *Region::findHostVar(void *var) {
  DEBUGLN(" Trying to find var with reference: %p ", (var));
  CorrMap::iterator iter;
  iter = _currentMap.find(var);
  if (iter == _currentMap.end()) {
    // TODO: handle errors
    std::cerr << "Error! host var for ptr: " << var << " not found "
              << std::endl;
    throw std::bad_exception();
    return NULL;
  } else {
    DEBUGLN(" OK");
    return iter->second;
  }
}

void Region::updateHostVar(HostVar *hostVar) {

  /* 1. Find equivalent device Var */
  CorrMap::iterator iter;
  Var *d_a = NULL;
  // iter = _deviceMap.find(hostVar);
  if (iter == _currentMap.end()) {
    /* 2. Create device var if  does not exist */
    d_a = _dev->createVar();
    d_a->setSize(hostVar->getSize());
    d_a->alloc();
    /*      HtD_pair d(hostVar, d_a);
            HtD_Map.append(d);*/
  } else {
    d_a = iter->second;
  }

  /* 3. Copy contents of hostVar to device Var if */
  d_a->copy_in(hostVar);
}

void Region::concyleAll() {
  DEBUGLN("Concyling all variables");
  /* Check dirty status of all variables*/
  // std::cout << "En hostVars hay " << hostVars.size () << " variables " <<
  // std::endl;
  // std::cout << "En _deviceMap hay " << _deviceMap.size () << " variables " <<
  // std::endl;

  HtD_Map::iterator iter;
  for (iter = _deviceMap.begin(); iter != _deviceMap.end(); iter++) {
    std::cout << "Cojo a " << iter->second << std::endl;

    // Search for the correspndant device var //
    // std::cout << "Vamos a sacar un puntero "  << " que apunta a " << *iter_h
    // << std::endl;
    if ((iter->first)->getDirection() != common::IN) {
      // DEBUGLN ("En el concyleAll hay una variable marcada como copy out de
      // tamaño %d \n", (*iter_h)->getSize());
      iter->second->copy_out(iter->first);
      // this->concyleHostVar(*iter_h);
    }
  }
}

void Region::concyleVar(void *var) {
  DEBUGLN("Concyle of var %p \n", var);
  /* Search for a particular host var */
  /* HostVar * hvar = findHostVar(var);
     this->concyleHostVar(hvar); */
  Var *dVar = findDeviceVar(var);
  Var *hVar = findHostVar(var);
  if (dVar->isDirty()) {
    // D to H
    dVar->copy_out(hVar);
    dVar->clean();
  } else {
    // H to D
    dVar->copy_in(hVar);
  }
}

/* concyleHostVar
 * ===================================
 *
 *   Concyle a particular host var with each one
 *     of its copies in the Devices
 */
void Region::concyleHostVar(HostVar *hvar) {

  /* if dVar is dirty:
     -> HostVar is updated with
     dVar value
else:
-> dVar is updated with HostVar
value
   */
  Var *dvar;
  HtD_Map::iterator iter;
  iter = _deviceMap.find(hvar);
  if (iter == _deviceMap.end()) {
    // TODO: handle errors
    std::cerr << "Error! Hostvar does not exist" << hvar << std::endl;
    throw std::bad_exception();
  } else {
    dvar = iter->second;
    if (dvar->isDirty()) {
      DEBUGLN("Se hace copy out de una variable sucia. De esta: %p\n",
              hvar->getPtr());
      dvar->copy_out(hvar);
      dvar->clean();
      DEBUGLN("Se sale del copy out\n");
    } else {
      DEBUGLN(
          "Se hace copy in de una variable limpia. Por si se modificó fuera\n");
      dvar->copy_in(hvar);
    }
  }
}

int Region::searchKernel(std::string name) {
  Kernel_list::iterator it = _kernel_list.find(name);
  if (it == _kernel_list.end()) {
    _kernel_list.insert(name);
    return 0;
  } else
    return 1;
}

void Region::incNested() {
  ScopeType *st = new ScopeType;
  _scope.push_back(st);
  __nestedCall++;
}

void Region::removeVar(void *var) {
  DeviceMap::iterator diter;
  diter = _deviceVars.find(var);
  diter->second->free();
  if (diter != _deviceVars.end()) {
    _deviceVars.erase(diter);
  }
  CorrMap::iterator citer;
  citer = _currentMap.find(var);
  if (citer != _currentMap.end()) {
    HtD_Map::iterator iter;
    iter = _deviceMap.find(citer->second);
    if (iter != _deviceMap.end())
      _deviceMap.erase(iter);
    _currentMap.erase(citer);
  }
}

int Region::belongs_scope(void *var) {
  int i = __nestedCall - 2;
  for (i; i >= 0; i--) { // for all the scopes
    ScopeType *list = _scope[i];
    ScopeType::iterator it = list->begin();
    for (; it != list->end(); it++) {
      if (*it == var) {
        return i;
      }
    }
  }
  return -1;
}

void Region::decNested() {
  assert(__nestedCall > 0);
  ScopeType *list = _scope[(__nestedCall - 1)];
  ScopeType::iterator it = list->begin();
  // DEBUGLN ("# Se borra un contexto. Tengo que recorrer las variables a nivel
  // de anidamiento %d que suman un total de %d variables", __nestedCall -1,
  // list->size());
  if ((__nestedCall - 1) >= 0) {
    DEBUGLN(" This is a nested scope, lvl %d \n", __nestedCall - 1);
    for (; it != list->end(); it++) {
      common::Var *d_var = this->findDeviceVar((*it));
      common::HostVar *h_var = this->findHostVar((*it));
      DEBUGLN(" Copying out variable %p \n", h_var->getPtr());
      d_var->copy_out(h_var);
      int i;
      if (belongs_scope(*it) < 0) // is NOT in a previous region
        this->removeVar((*it)); //PABLO: Es posible que esto en el futuro haya que cambiarlo
        //porque me elimina la variable de un contexto que no tiene por qué
      // d_var->free();
      // Tengo que sacarla de la lista
    }
  } else {
    DEBUGLN("No se hace al ser el minimo nivel de anidamiento. Se encargara el "
            "destroyContext");
  }
  __nestedCall--;
  list->clear();
  _scope.pop_back();
}
}
