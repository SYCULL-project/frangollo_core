/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/*! \mainpage Frangollo Compile-time Library
 * @author Ruyman Reyes <rreyes@ull.es>
 * @author Ivan Lopez <ilopezro@ull.es>
 *
 * \section sec Introduction
 *
 * Expressing parallelism on the source code is the key for the success of
 * programming productivity. However, although relevant success have been made
 * by OpenMP and others, further research is to be done in the field of source
 * code expressiveness. With this goal in mind, we have developed YACF (Yet
 * Another Compiler Framework),  a compilation framework oriented to source to
 * source code translation, currently focused on heterogeneous High Performance
 * Computing. Particularly in its current state, the framework is aimed to
 * translate from llc to CUDA or OpenCL. llc is a high level parallel language
 * developed at La Laguna University which expresses parallelism through
 * compiler pragmas with the flavour of OpenMP. At this moment the YACF project
 * is at an inflection point and we are decoupling the translation process with
 * the introduction of ‘Frangollo’, a compile-time library that deals with the
 * final stage of code generation.<img
 * src="http://3.bp.blogspot.com/_IJ5x_Bl_hnY/TMmzCSH5V6I/AAAAAAAAQpY/VLZGMDI2awo/s400/frangollo.jpg"
 * align="RIGHT"></img>
 *
 *
 * \section sec1 llc
 *
 * <b>llc</b> is a lenguage with an OpenMP flavour...
 * \subsection subsection1 Pragmas
 *
 * Bla bla
 *
 * \subsection subsection2 Optimizations
 *
 * \section sec2 How to run Frangollo
 * bla bla
 */

#include <frangollo_core/System.h>
#include <frangollo_core/common/Device.h>
#include <frangollo_core/common/Region.h>
#include <frangollo_core/common/Var.h>
#include <frangollo_core/common/debugmacros.h>
#include "CL/sycl/accessor.hpp"
#include <list>
#include <string>
#include <vector>

typedef enum {
  sycl_device_none,
  sycl_device_default,
  sycl_device_host,
  sycl_device_not_host,
  sycl_device_nvidia,
  sycl_device_generic_opencl
} sycl_device_t;

/* Region identifier */
typedef int ctxtId_t;

typedef std::list<tkernel_list> KernelList;

typedef common::Region *RegionPtr;
typedef common::Device *DevicePtr;

namespace frgsycl {

// enum VarDirection { IN, OUT, INOUT, NONE };
const int IN = 0;
const int OUT = 1;
const int INOUT = 2;
const int NONE = 3;

// enum CopyType { FORCE_COPY_IN, COPY_FIRST_ONLY };
const int FORCE_COPY_IN = 0;
const int COPY_FIRST_ONLY = 1;

// enum VarType { SCALAR, ARRAY, ARRAY2D };
const int SCALAR = 0;
const int ARRAY = 1;
const int ARRAY2D = 2;

// 0 Read Only, 1 Writable
const int READ_ONLY = 0;
const int WRITABLE = 1;

class FrangolloRuntime {
  //live region pointer
  RegionPtr LiveRegion = NULL;
  // ID of the current active context
  ctxtId_t SyclActiveContextId;

  tparams *kernel_parameter_list = NULL;

  int KernelSize;

  public:

    FrangolloRuntime () {
      sycl_init();
    }
      
    ~FrangolloRuntime () {
      sycl_stop();
    }

    /*Initiates devices*/
    void sycl_init();
    /*Finish execution*/
    void sycl_stop();

    ctxtId_t createInitContext(std::string name);

    ctxtId_t createSimpleContext(std::string name);

    void addSingleKernel(std::string name);

    ctxtId_t getActiveContext();

    int setActiveContext(ctxtId_t contextId);

    void registerVar(void *var,    // Ptr to original var
                 ctxtId_t cid, // Context identification number
                 var_layout_t var_layout);

    void registerArrayVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                      int CanChange, int ElementSize, int xElements);

    void registerScalarVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                       int CanChange, int ElementSize);

    void register2DArrayVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                        int CanChange, int ElementSize, int xElements,
                        int yElements);
    
    int destroyContext(ctxtId_t id);

    int submit_kernel_auto(ctxtId_t regionID, char *kernelName, tparams *params,
                       int size_x, int size_y, int size_z);

    void addParamToList(tparams *&ParameterList, void *PtrToVar, int VarType,
              int CanChange, int VarSize, char *VarName);

    sycl_device_t get_device_type();

    void barrier();

    void addToListOfKernels(KernelList &kernel_name_list, std::string KernelName);

    void addKernels(KernelList kernel_name_list);

    ctxtId_t createContext(std::string name, KernelList kernel_name_list);

    tparams* getKernelParamList();

    void clearParameterList();

    int getKernelSize();
    
    void addParamToKernel(int Mode, int KernelHash, cl::sycl::accessor<float, 1, cl::sycl::access::mode::write, cl::sycl::access::target::global_buffer> Accesor);
    
    void addParamToKernel(int Mode, int KernelHash, cl::sycl::accessor<float, 1, cl::sycl::access::mode::read, cl::sycl::access::target::global_buffer> Accesor);
};

std::string demangle(const char* mangled);

} // end of namespace frgsycl