/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <typeinfo>
#include <cxxabi.h>
#include <memory>
#include <linux/limits.h>
#include <frangollo_core/cuda/CudaDevice.h>
#include <frangollo_core/interface/syclFunctions.h>

namespace frgsycl {

void FrangolloRuntime::sycl_init() {
  if (!common::system.is_started()) {
    common::system.init();
  }
  createInitContext("init");
}

// Disconnects this host thread from the accelerator device.
void FrangolloRuntime::sycl_stop() {
  // std::map<ctxtId_t, RegionPtr>::iterator iter;
  RegionPtr region = this->LiveRegion;
  if (region) {
    region->decNested();
    DEBUG("** Region %d finished \n", this->SyclActiveContextId);
    DEBUGLN("******** END OF INIT CONTEXT The context is being deleted");
    region->destroy();
    DEBUGLN("************ END OF INIT CONTEXT The context has been deleted");
    delete region;
    this->SyclActiveContextId = 0;
    this->LiveRegion = NULL;
  }
  else
    DEBUG("** INIT context %d already destroyed\n", this->SyclActiveContextId);
}

ctxtId_t FrangolloRuntime::createInitContext(std::string name) {
  RegionPtr region = this->LiveRegion;
  if (region) {
    
    region->incNested();
    return this->SyclActiveContextId;
  }
  DEBUGLN("********* INIT CONTEXT CREATED: %s.", name.c_str());
  region = new common::Region(name.c_str());
  this->LiveRegion = region;
  this->SyclActiveContextId = 1;
  return this->SyclActiveContextId;
}

ctxtId_t FrangolloRuntime::createSimpleContext(std::string name) {
  RegionPtr region = this->LiveRegion;
  std::string ContextName = "_" + name + "_context";

  DEBUGLN("********* NEW SIMPLE CONTEXT: %s. The kernel name is: %s", ContextName.c_str(),
          name.c_str());
  
  if (region) {
    DEBUGLN("We are in nested region %s ", region->getName().c_str());
    DEBUGLN("Nested createContext. The variable is incremented");
    addSingleKernel(name);
    region->incNested();
    this->SyclActiveContextId = this->SyclActiveContextId + 1;
    return this->SyclActiveContextId;
  }

  int id = 1;
  RegionPtr newRegion = new common::Region(name.c_str());
  DevicePtr device = newRegion->getDevice();
  this->LiveRegion = newRegion;
  this->SyclActiveContextId = id;

  addSingleKernel(name);
  return this->SyclActiveContextId;
}

// Adds kernel to current Region
void FrangolloRuntime::addSingleKernel(std::string name) {
  RegionPtr region = this->LiveRegion;
  DevicePtr device = region->getDevice();

  if (region == NULL) {
    std::cout << "Error! addKernel was called while there is not";
    std::cout << "any active kernel. " << std::endl;
    return;
  }

  DEBUGLN("Starting to load %s ", name.c_str());
  if (region->searchKernel(name) == 0) {
      common::TKernelInfo kInfo;
      kInfo.name = name;
      kInfo.numMemoryAccess = 10;
      kInfo.numFlop = 10;

      //Get PTX name from the executable filename
      char result[ PATH_MAX ];
      ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
      std::string PTXStr(result, (count > 0) ? count : 0);
      auto const pos = PTXStr.find_last_of('/');
      PTXStr = PTXStr.substr(pos+1) + "_kernels";

      device->kernelPreload(kInfo, PTXStr);
      DEBUGLN("kernel %s loaded", name.c_str());
      
    } else
      DEBUGLN("Skyping kernel %s because it already was loaded in the region",
              name.c_str());

}

ctxtId_t FrangolloRuntime::getActiveContext() { 
  return this->SyclActiveContextId; 
}

int FrangolloRuntime::setActiveContext(ctxtId_t contextId) {
  this->SyclActiveContextId = contextId;
  return getActiveContext();
}

void FrangolloRuntime::registerVar(void *var,    // Ptr to original var
                 ctxtId_t cid, // Context identification number
                 var_layout_t var_layout) {
  RegionPtr region = this->LiveRegion;
  common::VarDirection dir = (common::VarDirection)var_layout.VarDirection;
  int size = var_layout.x;
  if (var_layout.y > 0) // check if two dimensions
    size *= var_layout.y * var_layout.elemSize;

  common::HostVar *h_a = new common::HostVar(var, size, dir);
  region->addHostVar(h_a);

  region->addDeviceVar(var, size, var_layout);
}

void FrangolloRuntime::registerArrayVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                      int CanChange, int ElementSize, int xElements) {
  RegionPtr region = this->LiveRegion;
  common::VarDirection dir = (common::VarDirection)VarDirection;
  common::HostVar *h_a = new common::HostVar(var, ElementSize * xElements, dir);
  var_layout_t var_layout;

  var_layout.VarDirection = VarDirection;
  var_layout.CopyType = CopyType;
  var_layout.VarType =
      frgsycl::ARRAY; // we are creating a ARRAY frangollo variable
  var_layout.CanChange = CanChange;
  var_layout.x = ElementSize * xElements;
  var_layout.y = 0;
  var_layout.lda = ElementSize * xElements;
  var_layout.elemSize = ElementSize;

  region->addHostVar(h_a);
  region->addDeviceVar(var, var_layout.x, var_layout);
}

void FrangolloRuntime::registerScalarVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                       int CanChange, int ElementSize) {
  RegionPtr region = this->LiveRegion;
  common::VarDirection dir = (common::VarDirection)VarDirection;
  common::HostVar *h_a = new common::HostVar(var, ElementSize, dir);
  var_layout_t var_layout;

  var_layout.VarDirection = VarDirection;
  var_layout.CopyType = CopyType;
  var_layout.VarType =
      frgsycl::SCALAR; // we are creating a SCALAR frangollo variable
  var_layout.CanChange = CanChange;
  var_layout.x = ElementSize;
  var_layout.y = 0;
  var_layout.lda = ElementSize;
  var_layout.elemSize = ElementSize;

  region->addHostVar(h_a);
  region->addDeviceVar(var, var_layout.x, var_layout);
}

// this functions haven't been tested
void FrangolloRuntime::register2DArrayVar(void *var, ctxtId_t cid, int VarDirection, int CopyType,
                        int CanChange, int ElementSize, int xElements,
                        int yElements) {
  RegionPtr region = this->LiveRegion;
  common::VarDirection dir = (common::VarDirection)VarDirection;
  common::HostVar *h_a =
      new common::HostVar(var, ElementSize * xElements * yElements, dir);
  var_layout_t var_layout;

  var_layout.VarDirection = VarDirection;
  var_layout.CopyType = CopyType;
  var_layout.VarType =
      frgsycl::ARRAY2D; // we are creating a 2DARRAY frangollo variable
  var_layout.CanChange = CanChange;
  var_layout.x = ElementSize * xElements;
  var_layout.y = ElementSize * yElements;
  var_layout.lda = ElementSize * xElements * yElements;
  var_layout.elemSize = ElementSize;

  region->addHostVar(h_a);
  region->addDeviceVar(var, var_layout.lda, var_layout);
}

int FrangolloRuntime::destroyContext(ctxtId_t id) {
  RegionPtr region = this->LiveRegion;
  if (region) {
    this->SyclActiveContextId = this->SyclActiveContextId - 1;
    region->decNested();
    if (region->getNestedCalls() == 0) {
      DEBUGLN("******** END CONTEXT The context is being deleted");
      region->destroy();
      DEBUGLN("       ************ END CONTEXT The context has been deleted");
      this->LiveRegion = NULL;
    } else
      DEBUGLN("** end context. NestedCall is decremented");
  } else {
    DEBUGLN("** Region %d not found \n", id);
  }
  return 0;
}

int FrangolloRuntime::submit_kernel_auto(ctxtId_t regionID, char *kernelName, tparams *params,
                       int size_x, int size_y, int size_z) {
  RegionPtr region = this->LiveRegion;

  region->getDevice()->kernel_launch(region, kernelName, params, size_x, size_y,
                                     size_z);                  
  return 0;
}

// Function which inserts at the end of the parameter list
void FrangolloRuntime::addParamToList(tparams *&ParameterList, void *PtrToVar, int VarType,
              int CanChange, int VarSize, char *VarName) {
  tparams *NewParam = ((tparams *)malloc(sizeof(tparams)));
  tparams *ParamsPtr = ParameterList;

  NewParam->_base_ptr = PtrToVar;
  NewParam->type = VarType;
  NewParam->reduction = (char *)"";
  NewParam->identity = (char *)"0";
  NewParam->reductionType = (char *)"";
  NewParam->canChange = CanChange;
  NewParam->size = VarSize;
  NewParam->varName = VarName;
  NewParam->next = NULL;

  if ((!ParameterList)) { // if there are not params, insert in list
    ParameterList = NewParam;
    return;
  }
  while (ParamsPtr->next) {
    ParamsPtr = ParamsPtr->next;
  } // at the end of the bucle ParamsPrt will be the last element
  ParamsPtr->next = NewParam;
}

sycl_device_t FrangolloRuntime::get_device_type() {
  RegionPtr region = this->LiveRegion;
  DevicePtr device = region->getDevice();

  if (device->getDeviceString() == cuda::CUDA_STRING)
    return sycl_device_nvidia;

  // If not CUDA or OPENCL, there is no available device and code
  // must be run in host
  return sycl_device_host;
}

void FrangolloRuntime::barrier() {
  RegionPtr region = this->LiveRegion;
  if (region)
    region->getDevice()->barrier();
}

// Function which adds a given kernel name to a list of kernels
void FrangolloRuntime::addToListOfKernels(KernelList &kernel_name_list, std::string KernelName) {
  tkernel_list KernelToAdd;
  strcpy(KernelToAdd.kernel_name, KernelName.c_str());
  kernel_name_list.push_back(KernelToAdd);
}

// Adds kernels in list to current Region
void FrangolloRuntime::addKernels(KernelList kernel_name_list) {
  RegionPtr region = this->LiveRegion;
  DevicePtr device = region->getDevice();
  if (region == NULL) {
    std::cout << "Error! addKernel was called while there is not";
    std::cout << "any active kernel. " << std::endl;
    return;
  }

  for (KernelList::const_iterator Kernel = kernel_name_list.begin();
       Kernel != kernel_name_list.end(); ++Kernel) {
    DEBUGLN("Starting to load %s ", Kernel->kernel_name);
    if (region->searchKernel(Kernel->kernel_name) == 0) {
      common::TKernelInfo kInfo;
      kInfo.name = Kernel->kernel_name;
      kInfo.numMemoryAccess = Kernel->numMemoryAccess;
      kInfo.numFlop = Kernel->numFlop;
      device->kernelPreload(kInfo);
      DEBUGLN("kernel %s loaded", Kernel->kernel_name);
    } else
      DEBUGLN("Skyping kernel %s because it already was loaded in the region",
              Kernel->kernel_name);
  }
}

ctxtId_t FrangolloRuntime::createContext(std::string name, KernelList kernel_name_list) {
  RegionPtr region = this->LiveRegion;

  DEBUGLN("********* NEW CONTEXT: %s. The first kernel is: %s", name.c_str(),
          kernel_name_list.front().kernel_name);

  if (region) { // in case we are in a nested region
    DEBUGLN("We are in nested region %s ", region->getName().c_str());
    DEBUGLN("Nested createContext. The variable is incremented");
    addKernels(kernel_name_list);
    region->incNested();
    this->SyclActiveContextId = this->SyclActiveContextId + 1;
    return this->SyclActiveContextId;
  }

  int id = 1;
  RegionPtr newRegion = new common::Region(name.c_str());
  DevicePtr device = newRegion->getDevice();
  this->LiveRegion = newRegion;
  this->SyclActiveContextId = id;

  addKernels(kernel_name_list);

  return this->SyclActiveContextId;
}

tparams* FrangolloRuntime::getKernelParamList() {
  return this->kernel_parameter_list; 
}

void FrangolloRuntime::clearParameterList() {
  this->kernel_parameter_list = NULL;
}

int FrangolloRuntime::getKernelSize() {
  return this->KernelSize;
}

void FrangolloRuntime::addParamToKernel(int Mode, int KernelHash, cl::sycl::accessor<float, 1, cl::sycl::access::mode::write, cl::sycl::access::target::global_buffer> Accesor) {
  if (Mode == 0)  //first call to the function
    this->setActiveContext(createSimpleContext(std::to_string(KernelHash)));

  int AccesorSize = Accesor.get_range();
  this->KernelSize = AccesorSize;
  void *pointer = Accesor.get_pointer();
  registerArrayVar(pointer, getActiveContext(), frgsycl::IN,
                              frgsycl::FORCE_COPY_IN, frgsycl::WRITABLE,
                              sizeof(float), AccesorSize);
  addParamToList(this->kernel_parameter_list, pointer, frgsycl::ARRAY,
                 frgsycl::WRITABLE, sizeof(float) * AccesorSize,
                 NULL);  

  if (Mode == 2) {
    registerScalarVar(&AccesorSize, getActiveContext(), frgsycl::IN,
                      frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                      sizeof(int));
    addParamToList(this->kernel_parameter_list, &AccesorSize, frgsycl::SCALAR,
                   frgsycl::READ_ONLY, sizeof(int), NULL);
  } //last call to the function, insert dummy


}

void FrangolloRuntime::addParamToKernel(int Mode, int KernelHash, cl::sycl::accessor<float, 1, cl::sycl::access::mode::read, cl::sycl::access::target::global_buffer> Accesor) {
  if (Mode == 0)  //first call to the function
    this->setActiveContext(createSimpleContext(std::to_string(KernelHash)));

  int AccesorSize = Accesor.get_range();
  void *pointer = Accesor.get_pointer();
  registerArrayVar(pointer, getActiveContext(), frgsycl::IN,
                              frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                              sizeof(float), AccesorSize);
  addParamToList(this->kernel_parameter_list, pointer, frgsycl::ARRAY,
                 frgsycl::READ_ONLY, sizeof(float) * AccesorSize,
                 NULL);  

  if (Mode == 2) {
    registerScalarVar(&AccesorSize, getActiveContext(), frgsycl::IN,
                      frgsycl::FORCE_COPY_IN, frgsycl::READ_ONLY,
                      sizeof(int));
    addParamToList(this->kernel_parameter_list, &AccesorSize, frgsycl::SCALAR,
                   frgsycl::READ_ONLY, sizeof(int), NULL);
  } //last call to the function, insert dummy
}

std::string demangle(const char* mangled) {
  int status;
  std::unique_ptr<char[], void (*)(void*)> result(
    abi::__cxa_demangle(mangled, 0, 0, &status), std::free);
  return result.get() ? std::string(result.get()) : "error occurred";
}

} // end of namespace frgsycl