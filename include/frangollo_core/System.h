/*
Copyright 2012 (C) Francisco de Sande Gonzalez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cstdlib>
#include <frangollo_core/common/Device.h>
#include <iostream>
#include <sstream>
#include <vector>
//#include "tracing.h"

#ifndef SYSTEM_H
#define SYSTEM_H

namespace common {

const unsigned int MAX_ENV_VARS = 5u;

enum env_vars {
  FRANGOLLO_PLATFORM,
  FRANGOLLO_GRANULARITY_FACTOR,
  FRANGOLLO_THREAD_NUM,
  ACC_DEVICE,
  ACC_DEVICE_NUM
};

class System {

private:
  std::vector<Device *> _deviceList;
  char *env[MAX_ENV_VARS];

  // This should be something more flexible
  int cudaPos;
  int oclPos;
  bool _started;

public:
  System();
  ~System();

  Device *getDevice();
  bool is_started() { return _started; };
  void init();
  std::vector<Device *> getDeviceList();
  std::string getEnv(const enum env_vars var_name) {
    if (env[var_name])
      return std::string(env[var_name]);
    else
      return "";
  };
};
}
#endif
